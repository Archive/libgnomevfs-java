/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevfs;

import org.gnu.glib.Enum;

/**
 */
public class DeviceType extends Enum {
    static final private int _UNKNOWN = 0;
    static final public DeviceType UNKNOWN = new DeviceType(_UNKNOWN);

    static final private int _AUDIO_CD = 1;
    static final public DeviceType AUDIO_CD = new DeviceType(_AUDIO_CD);
    
    static final private int _VIDEO_DVD = 2;
    static final public DeviceType VIDEO_DVD = new DeviceType(_VIDEO_DVD);
    
    static final private int _HARDDRIVE = 3;
    static final public DeviceType HARDDRIVE = new DeviceType(_HARDDRIVE);
    
    static final private int _CDROM = 4;
    static final public DeviceType CDROM = new DeviceType(_CDROM);
    
    static final private int _FLOPPY = 5;
    static final public DeviceType FLOPPY = new DeviceType(_FLOPPY);
    
    static final private int _ZIP = 6;
    static final public DeviceType ZIP = new DeviceType(_ZIP);
    
    static final private int _JAZ = 7;
    static final public DeviceType JAZ = new DeviceType(_JAZ);
    
    static final private int _NFS = 8;
    static final public DeviceType NFS = new DeviceType(_NFS);
    
    static final private int _AUTOFS = 9;
    static final public DeviceType AUTOFS = new DeviceType(_AUTOFS);
    
    static final private int _CAMERA = 10;
    static final public DeviceType CAMERA = new DeviceType(_CAMERA);
    
    static final private int _MEMORY_STICK = 11;
    static final public DeviceType MEMORY_STICK = new DeviceType(_MEMORY_STICK);
    
    static final private int _SMB = 12;
    static final public DeviceType SMB = new DeviceType(_SMB);
    
    static final private int _APPLE = 13;
    static final public DeviceType APPLE = new DeviceType(_APPLE);
    
    static final private int _MUSIC_PLAYER = 14;
    static final public DeviceType MUSIC_PLAYER = new DeviceType(_MUSIC_PLAYER);

    static final private int _WINDOWS = 15;
    static final public DeviceType WINDOWS = new DeviceType(_WINDOWS);
    
    static final private int _LOOPBACK = 16;
    static final public DeviceType LOOPBACK = new DeviceType(_LOOPBACK);
    
    static final private int _NETWORK = 17;
    static final public DeviceType NETWORK = new DeviceType(_NETWORK);
    
    static final private DeviceType[] theInterned = new DeviceType[] {
    	UNKNOWN, AUDIO_CD, VIDEO_DVD, HARDDRIVE, CDROM, FLOPPY, ZIP,
    	JAZ, NFS, AUTOFS, CAMERA, MEMORY_STICK, SMB, APPLE, MUSIC_PLAYER,
    	WINDOWS, LOOPBACK, NETWORK
    };

    static private java.util.Hashtable theInternedExtras;

    static final private DeviceType theSacrificialOne = new DeviceType(0);

    static public DeviceType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        DeviceType already = (DeviceType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new DeviceType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private DeviceType(int value) {
        value_ = value;
    }

    public boolean test(DeviceType other) {
        return (value_ & other.value_) == other.value_;
    }
}
