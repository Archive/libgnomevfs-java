/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevfs;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 * Object for handing directory related tasks.
 */
public class Directory extends GObject {
	
	public Directory(String uri, FileInfoOptions options) throws VFSException {
		Handle handle = getNullHandle();
		Result result = Result.intern(gnome_vfs_directory_open(handle, uri, options.getValue()));
		if (result == Result.OK)
			setHandle(handle);
		else
			throw new VFSException(result);
	}
	
	public Directory(URI uri, FileInfoOptions options) throws VFSException {
		Handle handle = GObject.getNullHandle();
		Result result = Result.intern(gnome_vfs_directory_open_from_uri(handle, uri.getHandle(), options.getValue()));
		if (result == Result.OK)
			setHandle(handle);
		else
			throw new VFSException(result);
	}
	
	public void close() throws VFSException {
		Result result = Result.intern(gnome_vfs_directory_close(getHandle()));
		if (result != Result.OK)
			throw new VFSException(result);
	}
	
	public FileInfo readNext() throws VFSException {
		Handle info = GObject.getNullHandle();
		Result result = Result.intern(gnome_vfs_directory_read_next(getHandle(), info));
		if (result == Result.OK)
			return new FileInfo(info);
		else
			throw new VFSException(result);
	}
	
	/**
	 * Create a new directory
	 * 
	 * @param uri URI of the directory to be created.
	 * @param perm Unix-style permissions for the newly created directory.
	 */
	static public void makeDirectory(String uri, int perm) throws VFSException{
		Result result =  Result.intern(gnome_vfs_make_directory(uri, perm));
		if (result != Result.OK)
			throw new VFSException(result);
	}
	
	/**
	 * Create a directory at uri.  Only succeeds if a file or directory does
	 * not already exist at uri.
	 * 
	 * @param uri
	 * @param perm
	 */
	static public void makeDirectory(URI uri, int perm) throws VFSException {
		Result result =  Result.intern(gnome_vfs_make_directory_for_uri(uri.getHandle(), perm));
		if (result != Result.OK)
			throw new VFSException(result);
	}
	
	/**
	 * Remove the directory.  The directory must be empty.
	 * 
	 * @param uri
	 */
	static public void removeDirectory(String uri) throws VFSException {
		Result result = Result.intern(gnome_vfs_remove_directory(uri));
		if (result != Result.OK)
			throw new VFSException(result);
	}
	
	/**
	 * Remove the directory.  The directory must be empty.
	 * 
	 * @param uri
	 */
	static public void removeDirectory(URI uri) throws VFSException {
		Result result = Result.intern(gnome_vfs_remove_directory_from_uri(uri.getHandle()));
		if (result != Result.OK)
			throw new VFSException(result);
	}


	/**
	 * Used to return well known directories such as Trash, Desktop, etc. from different 
	 * file systems.
	 * <p>
	 * There is quite a complicated logic behind finding/creating a Trash directory 
	 * and you need to be aware of some implications: Finding the Trash the first 
	 * time when using the file method may be pretty expensive. A cache file is 
	 * used to store the location of that Trash file for next time. If ceateIfNeeded 
	 * is specified without findIfNeeded, you may end up creating a Trash file when 
	 * there already is one. Your app should start out by doing a gnome_vfs_find_directory 
	 * with the findIfNeeded to avoid this and then use the createIfNeeded flag to 
	 * create Trash lazily when it is needed for throwing away an item on a given 
	 * disk.
	 * 
	 * @param nearURI find a well known directory on the same volume as nearUri
	 * @param kind kind of well known directory
	 * @param createIfNeeded If directory we are looking for does not exist, try 
	 * to create it
	 * @param findIfNeeded If we don't know where trash is yet, look for it.
	 * @param perms If creating, use these permissions
	 */
	static public URI findDirectory(URI nearURI, FindDirectoryKind kind,
				boolean createIfNeeded, boolean findIfNeeded, FilePermissions perms) throws VFSException {
		Handle hndl = GObject.getNullHandle();
		Result result = Result.intern(gnome_vfs_find_directory(nearURI.getHandle(), kind.getValue(),
					hndl, createIfNeeded, findIfNeeded, perms.getValue()));
		if (result != Result.OK)
			throw new VFSException(result);
		return new URI(hndl);
	}
	
	native static final protected int gnome_vfs_directory_open(Handle handle, String uri, int options);
	native static final protected int gnome_vfs_directory_open_from_uri(Handle handle, Handle uri, int options);
	native static final protected int gnome_vfs_directory_read_next(Handle handle, Handle fileInfo);
	native static final protected int gnome_vfs_directory_close(Handle handle);
	// static public API
    native static final protected int gnome_vfs_make_directory(String uri, int perm);
    native static final protected int gnome_vfs_make_directory_for_uri(Handle uri, int perm);
    native static final protected int gnome_vfs_remove_directory(String uri);
    native static final protected int gnome_vfs_remove_directory_from_uri(Handle uri);
    native static final protected int gnome_vfs_find_directory(Handle nearUri, int kind, Handle result, boolean create,
    		boolean find, int permissions);
}
