/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevfs;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 */
public class Drive extends GObject {
	
	Drive(Handle hndl) {
		super(hndl);
	}

	public long getID() {
		return gnome_vfs_drive_get_id(getHandle());
	}

	public DeviceType getDeviceType() {
		return DeviceType.intern(gnome_vfs_drive_get_device_type(getHandle()));
	}

	public Volume[] getMountedVolumes() {
		Handle[] hndls = gnome_vfs_drive_get_mounted_volumes(getHandle());
		if (hndls == null || hndls.length == 0)
			return null;
		Volume[] vols = new Volume[hndls.length];
		for (int i = 0; i < hndls.length; i++)
			vols[i] = new Volume(hndls[i]);
		return vols;
	}

	public String getDevicePath() {
		return gnome_vfs_drive_get_device_path(getHandle());
	}

	public String getActivationURI() {
		return gnome_vfs_drive_get_activation_uri(getHandle());
	}

	public String getDisplayName() {
		return gnome_vfs_drive_get_display_name(getHandle());
	}

	public String getIcon() {
		return gnome_vfs_drive_get_icon(getHandle());
	}

	public String getHalUdi() {
		return gnome_vfs_drive_get_hal_udi(getHandle());
	}

	public boolean isUserVisible() {
		return gnome_vfs_drive_is_user_visible(getHandle());
	}

	public boolean isConnected() {
		return gnome_vfs_drive_is_connected(getHandle());
	}

	public boolean isMounted() {
		return gnome_vfs_drive_is_mounted(getHandle());
	}

	static public int compare(Drive a, Drive b) {
		return gnome_vfs_drive_compare(a.getHandle(), b.getHandle());
	}

	/*
	 * gnome_vfs_drive_mount callback
	 */
	private void mount(boolean success, String error, String detailError) {
	}
	
	/*
	 * gnome_vfs_drive_unmount callback
	 */
	private void unmount(boolean success, String error, String detailError) {
	}

	/*
	 * gnome_vfs_drive_eject callback
	 */
	private void eject(boolean success, String error, String detailError) {
	}

	native static final private int gnome_vfs_drive_get_type();
	native static final private Handle gnome_vfs_drive_ref(Handle drive);
	native static final private void gnome_vfs_drive_unref(Handle drive);
	native static final private void gnome_vfs_drive_volume_list_free(Handle[] vols);
	native static final private long gnome_vfs_drive_get_id(Handle drive);
	native static final private int gnome_vfs_drive_get_device_type(Handle drive);
	native static final private Handle[] gnome_vfs_drive_get_mounted_volumes(Handle drive);
	native static final private String gnome_vfs_drive_get_device_path(Handle drive);
	native static final private String gnome_vfs_drive_get_activation_uri(Handle drive);
	native static final private String gnome_vfs_drive_get_display_name(Handle drive);
	native static final private String gnome_vfs_drive_get_icon(Handle drive);
	native static final private String gnome_vfs_drive_get_hal_udi(Handle drive);
	native static final private boolean gnome_vfs_drive_is_user_visible(Handle drive);
	native static final private boolean gnome_vfs_drive_is_connected(Handle drive);
	native static final private boolean gnome_vfs_drive_is_mounted(Handle drive);
	native static final private int gnome_vfs_drive_compare(Handle da, Handle db);
}
