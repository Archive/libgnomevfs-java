/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevfs;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;
import org.gnu.glib.Struct;

/**
 */
public class File extends Struct {
    
    public File(Handle handle) {
        super(handle);
    }
    
    static public File create(String uri, OpenMode openMode, boolean exclusive, FilePermissions perms) throws VFSException {
        Handle hndl = GObject.getNullHandle();
        checkResults(gnome_vfs_create(hndl, uri, openMode.getValue(), exclusive, perms.getValue()));
        return new File(hndl);
    }
    
    static public File create(URI uri, OpenMode openMode, boolean exclusive, FilePermissions perms) throws VFSException {
        Handle hndl = GObject.getNullHandle();
        checkResults(gnome_vfs_create_uri(hndl, uri.getHandle(), openMode.getValue(), exclusive, perms.getValue()));
        return new File(hndl);
        
    }

    static public File open(String uri, OpenMode openMode) throws VFSException {
        Handle hndl = GObject.getNullHandle();
        checkResults(gnome_vfs_open(hndl, uri, openMode.getValue()));
        return new File(hndl);
    }
    
    static public File open(URI uri, OpenMode openMode) throws VFSException {
        Handle hndl = GObject.getNullHandle();
        checkResults(gnome_vfs_open_uri(hndl, uri.getHandle(), openMode.getValue()));
        return new File(hndl);
    }
    
    static public void unlink(String uri) throws VFSException {
        checkResults(gnome_vfs_unlink(uri));
    }
    
    static public void unlink(URI uri) throws VFSException {
        checkResults(gnome_vfs_unlink_from_uri(uri.getHandle()));
    }
    
    static public void move(URI olduri, URI newuri, boolean forceReplace) throws VFSException {
        checkResults(gnome_vfs_move_uri(olduri.getHandle(), newuri.getHandle(), forceReplace));
    }
    
    static public void move(String olduri, String newuri, boolean forceReplace) throws VFSException {
        checkResults(gnome_vfs_move(olduri, newuri, forceReplace));
    }
    
    static public boolean sameFileSystem(URI source, URI target) throws VFSException {
        boolean[] same = new boolean[1];
        checkResults(gnome_vfs_check_same_fs_uris(source.getHandle(), target.getHandle(), same));
        return same[0];
    }
    
    static public boolean sameFileSystem(String source, String target) throws VFSException {
        boolean[] same = new boolean[1];
        checkResults(gnome_vfs_check_same_fs(source, target, same));
        return same[0];
    }
    
    static public boolean exists(URI uri)  {
        return gnome_vfs_uri_exists(uri.getHandle());
    }
    
    static public void createSymbolicLink(URI uri, String targetReference) throws VFSException {
        checkResults(gnome_vfs_create_symbolic_link(uri.getHandle(), targetReference));
    }
    

    public void close() throws VFSException {
        checkResults(gnome_vfs_close(getHandle()));
    }
    
    public byte[] read(long bytes) throws VFSException {
        byte[] results = new byte[1];
        long[] size = new long[1];
        checkResults(gnome_vfs_read(getHandle(), results, bytes, size));
        return results;
    }

    public long write(byte[] buffer, long numBytes) throws VFSException {
        long[] size = new long[1];
        checkResults(gnome_vfs_write(getHandle(), buffer, numBytes, size));
        return size[0]; 
    }
    
    public void seek(SeekPosition whence, long offset) throws VFSException {
        checkResults(gnome_vfs_seek(getHandle(), whence.getValue(), offset));
    }
    
    public long tell() throws VFSException {
        long[] offset = new long[1];
        checkResults(gnome_vfs_tell(getHandle(), offset));
        return offset[0];
    }
    
    
    
    
    static private void checkResults(int results) throws VFSException {
        Result result = Result.intern(results);
        if (Result.OK != result)
            throw new VFSException(result);
    }
    

	// basic file operations
    native static final protected int gnome_vfs_create(Handle handle, String uri, int openMode, boolean exclusive, int perm);
    native static final protected int gnome_vfs_create_uri(Handle handle, Handle uri, int openMode, boolean exclusive, int perm);
    native static final protected int gnome_vfs_open(Handle handle, String uri, int openMode);
    native static final protected int gnome_vfs_open_uri(Handle handle, Handle uir, int openMode);
    native static final protected int gnome_vfs_close(Handle handle);
    native static final protected int gnome_vfs_unlink(String uri);
    native static final protected int gnome_vfs_unlink_from_uri(Handle uri);
    native static final protected int gnome_vfs_move_uri(Handle oldUri, Handle newUri, boolean forceReplace);
    native static final protected int gnome_vfs_move(String oldUri, String newUri, boolean forceReplace);
    native static final protected int gnome_vfs_check_same_fs_uris(Handle source, Handle target, boolean[] sameFS);
    native static final protected int gnome_vfs_check_same_fs(String source, String target, boolean[] sameFS);
    native static final protected boolean gnome_vfs_uri_exists(Handle uri);
    native static final protected int gnome_vfs_create_symbolic_link(Handle uri, String targetReference);
    // reading and writing
    native static final protected int gnome_vfs_read(Handle handle, byte[] buffer, long size, long[] read);
    native static final protected int gnome_vfs_write(Handle handle, byte[] buffer, long size, long[] written);
    native static final protected int gnome_vfs_seek(Handle handle, int seekPos, long offset);
    native static final protected int gnome_vfs_tell(Handle handle, long[] offset);
    // getting and setting file information
    
}
