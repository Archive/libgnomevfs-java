/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevfs;

import org.gnu.glib.Flags;

/**
 */
public class FileFlags extends Flags {
    static final private int _NONE = 0;
    /** No flags */
    static final public FileFlags NONE = new FileFlags(_NONE);

    static final private int _SYMLINK = 1 << 0;
    /** File is a symlink */
    static final public FileFlags SYMLINK = new FileFlags(_SYMLINK);

    static final private int _LOCAL = 1 << 1;
    /** File is on local filesystem */
    static final public FileFlags LOCAL = new FileFlags(_LOCAL);

    static final private FileFlags[] theInterned = new FileFlags[] { 
    		NONE, SYMLINK, LOCAL
	};

    static private java.util.Hashtable theInternedExtras;

    static final private FileFlags theSacrificialOne = new FileFlags(0);

    static public FileFlags intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        FileFlags already = (FileFlags) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new FileFlags(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private FileFlags(int value) {
        value_ = value;
    }

    public FileFlags or(FileFlags other) {
        return intern(value_ | other.value_);
    }

    public FileFlags and(FileFlags other) {
        return intern(value_ & other.value_);
    }

    public FileFlags xor(FileFlags other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(FileFlags other) {
        return (value_ & other.value_) == other.value_;
    }
}
