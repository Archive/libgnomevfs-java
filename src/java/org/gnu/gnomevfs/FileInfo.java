/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevfs;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 * Contains information about a file.
 */
public class FileInfo extends GObject {
	
    public FileInfo() {
        super(gnome_vfs_file_info_new());
    }
    
    FileInfo(Handle handle) {
		super(handle);
	}
    
    public void clear() {
        gnome_vfs_file_info_clear(getHandle());
    }
    
    public String getMimeType() {
        return gnome_vfs_file_info_get_mime_type(getHandle());
    }
    
    public boolean matches(FileInfo other) {
        return gnome_vfs_file_info_matches(getHandle(), other.getHandle());
    }
    
    public String getName() {
        return getName(getHandle());
    }
    
    public FileInfoFields getValidFields() {
        return FileInfoFields.intern(getValidFields(getHandle()));
    }
    
    public FileType getFileType() {
        return FileType.intern(getFileType(getHandle()));
    }
    
    public FilePermissions getFilePermissions() {
        return FilePermissions.intern(getFilePermissions(getHandle()));
    }
    
    public FileFlags getFileFlags() {
        return FileFlags.intern(getFileFlags(getHandle()));
    }
    
    public long getFileSize() {
        return getFileSize(getHandle());
    }
    
    public long getBlockCount() {
        return getBlockCount(getHandle());
    }
    
    public String getSymlinkName() {
        return getSymlinkName(getHandle());
    }

    public long getAccessTime() {
        return getAtime(getHandle());
    }
    
    public long getModifiedTime() {
        return getMtime(getHandle());
    }
    
    public long getCreationTime() {
        return getCtime(getHandle());
    }

	native static final protected Handle gnome_vfs_file_info_new();
	native static final protected void gnome_vfs_file_info_clear(Handle fileinfo);
	native static final protected String gnome_vfs_file_info_get_mime_type(Handle fileinfo);
	native static final protected boolean gnome_vfs_file_info_matches(Handle a, Handle b);
	native static final protected String getName(Handle handle);
	native static final protected int getValidFields(Handle handle);
	native static final protected int getFileType(Handle handle);
	native static final protected int getFilePermissions(Handle handle);
	native static final protected int getFileFlags(Handle handle);
	native static final protected long getFileSize(Handle handle);
	native static final protected long getBlockCount(Handle handle);
	native static final protected String getSymlinkName(Handle handle);
	native static final protected long getAtime(Handle handle);
	native static final protected long getCtime(Handle handle);
	native static final protected long getMtime(Handle handle);

}
