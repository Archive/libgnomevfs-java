/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevfs;

import org.gnu.glib.Flags;

/**
 * Flags indicating what fields in a VFSFileInfo object are valid.  Name
 * is always assumed valid.
 */
public class FileInfoFields extends Flags {
	
    static final private int _NONE = 0;
    /** No fields are valid */
    static final public FileInfoFields NONE = new FileInfoFields(_NONE);

    static final private int _TYPE = 1 << 0;
    /** Type field is valid */
    static final public FileInfoFields TYPE = new FileInfoFields(_TYPE);

    static final private int _PERMISSIONS = 1 << 1;
    /** Permissions field is valid */
    static final public FileInfoFields PERMISSIONS = new FileInfoFields(_PERMISSIONS);

    static final private int _FLAGS = 1 << 2;
    /** Flags field is valid */
    static final public FileInfoFields FLAGS = new FileInfoFields(_FLAGS);

    static final private int _DEVICE = 1 << 3;
    /** Device field is valid */
    static final public FileInfoFields DEVICE = new FileInfoFields(_DEVICE);

    static final private int _INODE = 1 << 4;
    /** Inode field is valid */
    static final public FileInfoFields INODE = new FileInfoFields(_INODE);

    static final private int _LINK_COUNT = 1 << 5;
    /** Link cound field is valid */
    static final public FileInfoFields LINK_COUNT = new FileInfoFields(_LINK_COUNT);

    static final private int _SIZE = 1 << 6;
    /** Size field is valid */
    static final public FileInfoFields SIZE = new FileInfoFields(_SIZE);

    static final private int _BLOCK_COUNT = 1 << 7;
    /** Block count field is valid */
    static final public FileInfoFields BLOCK_COUNT = new FileInfoFields(_BLOCK_COUNT);

    static final private int _IO_BLOCK_SIZE = 1 << 8;
    /** I/O block size field is valid */
    static final public FileInfoFields IO_BLOCK_SIZE = new FileInfoFields(_IO_BLOCK_SIZE);

    static final private int _ATIME = 1 << 9;
    /** Access time field is valid */
    static final public FileInfoFields ATIME = new FileInfoFields(_ATIME);

    static final private int _MTIME = 1 << 10;
    /** Modification time field is valid */
    static final public FileInfoFields MTIME = new FileInfoFields(_MTIME);

    static final private int _CTIME = 1 << 11;
    /** Creation time field is valid */
    static final public FileInfoFields CTIME = new FileInfoFields(_CTIME);

    static final private int _SYMLINK_NAME = 1 << 12;
    /** Symlink name field is valid */
    static final public FileInfoFields SYMLINK_NAME = new FileInfoFields(_SYMLINK_NAME);

    static final private int _MIME_TYPE = 1 << 13;
    /** Mime type field is valid */
    static final public FileInfoFields MIME_TYPE = new FileInfoFields(_MIME_TYPE);

    static final private int _ACCESS = 1 << 14;
    /** Access bits of the permissions bitfield are valid */
    static final public FileInfoFields ACCESS = new FileInfoFields(_ACCESS);

    static final private FileInfoFields[] theInterned = new FileInfoFields[] { 
    		NONE, FLAGS, DEVICE, INODE, LINK_COUNT, SIZE, BLOCK_COUNT, IO_BLOCK_SIZE, 
			ATIME, MTIME, CTIME, SYMLINK_NAME, MIME_TYPE, ACCESS };

    static private java.util.Hashtable theInternedExtras;

    static final private FileInfoFields theSacrificialOne = new FileInfoFields(0);

    static public FileInfoFields intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        FileInfoFields already = (FileInfoFields) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new FileInfoFields(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private FileInfoFields(int value) {
        value_ = value;
    }

    public FileInfoFields or(FileInfoFields other) {
        return intern(value_ | other.value_);
    }

    public FileInfoFields and(FileInfoFields other) {
        return intern(value_ & other.value_);
    }

    public FileInfoFields xor(FileInfoFields other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(FileInfoFields other) {
        return (value_ & other.value_) == other.value_;
    }
}
