/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevfs;

import org.gnu.glib.Flags;

/**
 *  
 */
public class FileInfoOptions extends Flags {
    static final private int _DEFAULT = 0;

    static final public FileInfoOptions DEFAULT = new FileInfoOptions(_DEFAULT);

    static final private int _GET_MIME_TYPE = 1 << 0;
    /** Detect the MIME type */
    static final public FileInfoOptions GET_MIME_TYPE = new FileInfoOptions(_GET_MIME_TYPE);

    static final private int _FORCE_FAST_MIME_TYPE = 1 << 1;
    /** Only use fast MIME type detection */
    static final public FileInfoOptions FORCE_FAST_MIME_TYPE = new FileInfoOptions(_FORCE_FAST_MIME_TYPE);

    static final private int _FORCE_SLOW_MIME_TYPE = 1 << 2;
    /** Force slow MIME type detection where available */
    static final public FileInfoOptions FORCE_SLOW_MIME_TYPE = new FileInfoOptions(_FORCE_SLOW_MIME_TYPE);

    static final private int _FOLLOW_LINKS = 1 << 3;
    /** Automatically follow symbolic links and retrieve the peoperties of their target */
    static final public FileInfoOptions FOLLOW_LINKS = new FileInfoOptions(_FOLLOW_LINKS);

    static final private int _GET_ACCESS_RIGHTS = 1 << 4;
    /** Try to get data similar to what would be returned by access(2) on a local filesystem */
    static final public FileInfoOptions GET_ACCESS_RIGHTS = new FileInfoOptions(_GET_ACCESS_RIGHTS);

    static final private FileInfoOptions[] theInterned = new FileInfoOptions[] { 
    		DEFAULT, GET_MIME_TYPE, FORCE_FAST_MIME_TYPE, FORCE_SLOW_MIME_TYPE, FOLLOW_LINKS,  
    		GET_ACCESS_RIGHTS};

    static private java.util.Hashtable theInternedExtras;

    static final private FileInfoOptions theSacrificialOne = new FileInfoOptions(0);

    static public FileInfoOptions intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        FileInfoOptions already = (FileInfoOptions) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new FileInfoOptions(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private FileInfoOptions(int value) {
        value_ = value;
    }

    public FileInfoOptions or(FileInfoOptions other) {
        return intern(value_ | other.value_);
    }

    public FileInfoOptions and(FileInfoOptions other) {
        return intern(value_ & other.value_);
    }

    public FileInfoOptions xor(FileInfoOptions other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(FileInfoOptions other) {
        return (value_ & other.value_) == other.value_;
    }
}
