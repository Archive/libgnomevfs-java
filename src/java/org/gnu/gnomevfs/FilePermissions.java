/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevfs;

import org.gnu.glib.Enum;

/**
 * File permissions.  These are the same as the Unix ones.
 */
public class FilePermissions extends Enum {
    static final private int _SUID = getSUID();
    /** UID bit */
    static final public FilePermissions SUID = new FilePermissions(_SUID);

    static final private int _SGID = getSGID();
    /** GID bit */
    static final public FilePermissions SGID = new FilePermissions(_SGID);
    
    static final private int _STICKY = 01000;
    /** Sticky bit */
    static final public FilePermissions SITCKY = new FilePermissions(_STICKY);
    
    static final private int _USER_READ = getRUSR();
    /** Owner has read permission */
    static final public FilePermissions USER_READ = new FilePermissions(_USER_READ);
    
    static final private int _USER_WRITE = getWUSR();
    /** Owner has write permission */
    static final public FilePermissions USER_WRITE = new FilePermissions(_USER_WRITE);
    
    static final private int _USER_EXEC = getXUSR();
    /** Owner has execution permission */
    static final public FilePermissions USER_EXEC = new FilePermissions(_USER_EXEC);
    
    static final private int _USER_ALL = _USER_READ | _USER_WRITE | _USER_EXEC;
    /** Owner has all permissions */
    static final public FilePermissions USER_ALL = new FilePermissions(_USER_ALL);
    
    static final private int _GROUP_READ = getRGRP();
    /** Group has read permission */
    static final public FilePermissions GROUP_READ = new FilePermissions(_GROUP_READ);
    
    static final private int _GROUP_WRITE = getWGRP();
    /** Group has write permission */
    static final public FilePermissions GROUP_WRITE = new FilePermissions(_GROUP_WRITE);
    
    static final private int _GROUP_EXEC = getXGRP();
    /** Group has execution permission */
    static final public FilePermissions GROUP_EXEC = new FilePermissions(_GROUP_EXEC);
    
    static final private int _GROUP_ALL = _GROUP_READ | _GROUP_WRITE | _GROUP_EXEC;
    /** Group has all permissions */
    static final public FilePermissions GROUP_ALL = new FilePermissions(_GROUP_ALL);
    
    static final private int _OTHER_READ = getROTH();
    /** Others has read permission */
    static final public FilePermissions OTHER_READ = new FilePermissions(_OTHER_READ);
    
    static final private int _OTHER_WRITE = getWOTH();
    /** Others has write permission */
    static final public FilePermissions OTHER_WRITE = new FilePermissions(_OTHER_WRITE);
    
    static final private int _OTHER_EXEC = getXOTH();
    /** Others has execution permission */
    static final public FilePermissions OTHER_EXEC = new FilePermissions(_OTHER_EXEC);
    
    static final private int _OTHER_ALL = _OTHER_READ | _OTHER_WRITE | _OTHER_EXEC;
    /** Others has all permissions */
    static final public FilePermissions OTHER_ALL = new FilePermissions(_OTHER_ALL);

    static final private int _ACCESS_READABLE = 1 << 16;
    
    static final public FilePermissions ACCESS_READABLE = new FilePermissions(_ACCESS_READABLE);
    
    static final private int _ACCESS_WRITABLE = 1 << 17;
    
    static final public FilePermissions ACCESS_WRITABLE = new FilePermissions(_ACCESS_WRITABLE);
    
    static final private int _ACCESS_EXECUTABLE = 1 << 18;
    
    static final public FilePermissions ACCESS_EXECUTABLE = new FilePermissions(_ACCESS_EXECUTABLE);
    
    
    static final private FilePermissions[] theInterned = new FilePermissions[] {
            SUID, SGID, USER_READ, USER_WRITE, USER_EXEC, USER_ALL, GROUP_READ,
            GROUP_WRITE, GROUP_EXEC, GROUP_ALL, OTHER_READ, OTHER_WRITE,
            OTHER_EXEC, OTHER_ALL, ACCESS_READABLE, ACCESS_WRITABLE, ACCESS_EXECUTABLE
    };

    static private java.util.Hashtable theInternedExtras;

    static final private FilePermissions theSacrificialOne = new FilePermissions(0);

    static public FilePermissions intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        FilePermissions already = (FilePermissions) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new FilePermissions(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private FilePermissions(int value) {
        value_ = value;
    }

    public FilePermissions or(FilePermissions other) {
        return intern(value_ | other.value_);
    }

    public FilePermissions and(FilePermissions other) {
        return intern(value_ & other.value_);
    }

    public FilePermissions xor(FilePermissions other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(FilePermissions other) {
        return (value_ & other.value_) == other.value_;
    }

	native static final protected int getSUID();
	native static final protected int getSGID();
	native static final protected int getRUSR();
	native static final protected int getWUSR();
	native static final protected int getXUSR();
	native static final protected int getRGRP();
	native static final protected int getWGRP();
	native static final protected int getXGRP();
	native static final protected int getROTH();
	native static final protected int getWOTH();
	native static final protected int getXOTH();
    
}
