/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevfs;

import org.gnu.glib.Enum;

/**
 * Identifies the kind of file represented by a VFSFileInfo.
 */
public class FileType extends Enum {
    static final private int _UNKNOWN = 0;
    static final public FileType UNKNOWN = new FileType(_UNKNOWN);

    static final private int _REGULAR = 1;
    static final public FileType REGULAR = new FileType(_REGULAR);

    static final private int _DIRECTORY = 2;
    static final public FileType DIRECTORY = new FileType(_DIRECTORY);

    static final private int _FIFO = 3;
    static final public FileType FIFO = new FileType(_FIFO);

    static final private int _SOCKET = 4;
    static final public FileType SOCKET = new FileType(_SOCKET);

    static final private int _CHARACTER_DEVICE = 5;
    static final public FileType CHARACTER_DEVICE = new FileType(_CHARACTER_DEVICE);

    static final private int _BLOCK_DEVICE = 6;
    static final public FileType BLOCK_DEVICE = new FileType(_BLOCK_DEVICE);

    static final private int _SYMBOLIC_LINK = 7;
    static final public FileType SYMBOLIC_LINK = new FileType(_SYMBOLIC_LINK);

    static final private FileType[] theInterned = new FileType[] {
    		REGULAR, DIRECTORY, FIFO, SOCKET, CHARACTER_DEVICE, 
    		BLOCK_DEVICE, SYMBOLIC_LINK 
    };

    static private java.util.Hashtable theInternedExtras;

    static final private FileType theSacrificialOne = new FileType(0);

    static public FileType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        FileType already = (FileType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new FileType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private FileType(int value) {
        value_ = value;
    }

    public FileType or(FileType other) {
        return intern(value_ | other.value_);
    }

    public FileType and(FileType other) {
        return intern(value_ & other.value_);
    }

    public FileType xor(FileType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(FileType other) {
        return (value_ & other.value_) == other.value_;
    }
}
