/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevfs;

import org.gnu.glib.Enum;

/**
 * 
 */
public class FindDirectoryKind extends Enum {
    static final private int _DESKTOP = 0;
    static final public FindDirectoryKind DESKTOP = new FindDirectoryKind(_DESKTOP);

    static final private int _TRASH = 1;
    static final public FindDirectoryKind TRASH = new FindDirectoryKind(_TRASH);

    static final private FindDirectoryKind[] theInterned = new FindDirectoryKind[] {
    		DESKTOP, TRASH
    };

    static private java.util.Hashtable theInternedExtras;

    static final private FindDirectoryKind theSacrificialOne = new FindDirectoryKind(0);

    static public FindDirectoryKind intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        FindDirectoryKind already = (FindDirectoryKind) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new FindDirectoryKind(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private FindDirectoryKind(int value) {
        value_ = value;
    }

    public boolean test(FindDirectoryKind other) {
        return (value_ & other.value_) == other.value_;
    }

}
