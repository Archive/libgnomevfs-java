/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevfs;

import org.gnu.glib.Flags;

/**
 * Mode in which files are opened. If RANDOM is not used the files must be
 * accessed sequentially.
 */
public class OpenMode extends Flags {

    static final private int _NONE = 0;

    static final public OpenMode NONE = new OpenMode(_NONE);

    static final private int _READ = 1 << 0;

    static final public OpenMode READ = new OpenMode(_READ);

    static final private int _WRITE = 1 << 1;

    static final public OpenMode WRITE = new OpenMode(_WRITE);

    static final private int _RANDOM = 1 << 2;

    static final public OpenMode RANDOM = new OpenMode(_RANDOM);

    static final private OpenMode[] theInterned = new OpenMode[] { NONE,
            READ, WRITE, RANDOM };

    static private java.util.Hashtable theInternedExtras;

    static final private OpenMode theSacrificialOne = new OpenMode(0);

    static public OpenMode intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        OpenMode already = (OpenMode) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new OpenMode(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private OpenMode(int value) {
        value_ = value;
    }

    public OpenMode or(OpenMode other) {
        return intern(value_ | other.value_);
    }

    public OpenMode and(OpenMode other) {
        return intern(value_ & other.value_);
    }

    public OpenMode xor(OpenMode other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(OpenMode other) {
        return (value_ & other.value_) == other.value_;
    }
}