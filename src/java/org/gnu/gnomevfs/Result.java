/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevfs;

import org.gnu.glib.Enum;

/**
 */
public class Result extends Enum {

    static final private int _OK = 0;

    static final public Result OK = new Result(_OK);

    static final private int _ERROR_NOT_FOUND = 1;

    static final public Result ERROR_NOT_FOUND = new Result(_ERROR_NOT_FOUND);

    static final private int _ERROR_GENERIC = 2;

    static final public Result ERROR_GENERIC = new Result(_ERROR_GENERIC);

    static final private int _ERROR_INTERNAL = 3;

    static final public Result ERROR_INTERNAL = new Result(_ERROR_INTERNAL);

    static final private int _ERROR_BAD_PARAMETERS = 4;

    static final public Result ERROR_BAD_PARAMETERS = new Result(_ERROR_BAD_PARAMETERS);

    static final private int _ERROR_NOT_SUPPORTED = 5;

    static final public Result ERROR_NOT_SUPPORTED = new Result(_ERROR_NOT_SUPPORTED);

    static final private int _ERROR_IO = 6;

    static final public Result ERROR_IO = new Result(_ERROR_IO);

    static final private int _ERROR_CORRUPTED_DATA = 7;

    static final public Result ERROR_CORRUPTED_DATA = new Result(_ERROR_CORRUPTED_DATA);

    static final private int _ERROR_WRONG_FORMAT = 8;

    static final public Result ERROR_WRONG_FORMAT = new Result(_ERROR_WRONG_FORMAT);

    static final private int _ERROR_BAD_FILE = 9;

    static final public Result ERROR_BAD_FILE = new Result(_ERROR_BAD_FILE);

    static final private int _ERROR_TOO_BIG = 10;

    static final public Result ERROR_TOO_BIG = new Result(_ERROR_TOO_BIG);

    static final private int _ERROR_NO_SPACE = 11;

    static final public Result ERROR_NO_SPACE = new Result(_ERROR_NO_SPACE);

    static final private int _ERROR_READ_ONLY = 12;

    static final public Result ERROR_READ_ONLY = new Result(_ERROR_READ_ONLY);

    static final private int _ERROR_INVALID_URI = 13;

    static final public Result ERROR_INVALID_URI = new Result(_ERROR_INVALID_URI);

    static final private int _ERROR_NOT_OPEN = 14;

    static final public Result ERROR_NOT_OPEN = new Result(_ERROR_NOT_OPEN);

    static final private int _ERROR_INVALID_OPEN_MODE = 15;

    static final public Result ERROR_INVALID_OPEN_MODE = new Result(_ERROR_INVALID_OPEN_MODE);

    static final private int _ERROR_ACCESS_DENIED = 16;

    static final public Result ERROR_ACCESS_DENIED = new Result(_ERROR_ACCESS_DENIED);

    static final private int _ERROR_TOO_MANY_OPEN_FILES = 17;

    static final public Result ERROR_TOO_MANY_OPEN_FILES = new Result(_ERROR_TOO_MANY_OPEN_FILES);

    static final private int _ERROR_EOF = 18;

    static final public Result ERROR_EOF = new Result(_ERROR_EOF);

    static final private int _ERROR_NOT_A_DIRECTORY = 19;

    static final public Result ERROR_NOT_A_DIRECTORY = new Result(_ERROR_NOT_A_DIRECTORY);

    static final private int _ERROR_IN_PROGRESS = 20;

    static final public Result ERROR_IN_PROGRESS = new Result(_ERROR_IN_PROGRESS);

    static final private int _ERROR_INTERRUPTED = 21;

    static final public Result ERROR_INTERRUPTED = new Result(_ERROR_INTERRUPTED);

    static final private int _ERROR_FILE_EXISTS = 22;

    static final public Result ERROR_FILE_EXISTS = new Result(_ERROR_FILE_EXISTS);

    static final private int _ERROR_LOOP = 23;

    static final public Result ERROR_LOOP = new Result(_ERROR_LOOP);

    static final private int _ERROR_NOT_PERMITTED = 24;

    static final public Result ERROR_NOT_PERMITTED = new Result(_ERROR_NOT_PERMITTED);

    static final private int _ERROR_IS_DIRECTORY = 25;

    static final public Result ERROR_IS_DIRECTORY = new Result(_ERROR_IS_DIRECTORY);

    static final private int _ERROR_NO_MEMORY = 26;

    static final public Result ERROR_NO_MEMORY = new Result(_ERROR_NO_MEMORY);

    static final private int _ERROR_HOST_NOT_FOUND = 27;

    static final public Result ERROR_HOST_NOT_FOUND = new Result(_ERROR_HOST_NOT_FOUND);

    static final private int _ERROR_INVALID_HOST_NAME = 28;

    static final public Result ERROR_INVALID_HOST_NAME = new Result(_ERROR_INVALID_HOST_NAME);

    static final private int _ERROR_HOST_HAS_NO_ADDRESS = 29;

    static final public Result ERROR_HOST_HAS_NO_ADDRESS = new Result(_ERROR_HOST_HAS_NO_ADDRESS);

    static final private int _ERROR_LOGIN_FAILED = 30;

    static final public Result ERROR_LOGIN_FAILED = new Result(_ERROR_LOGIN_FAILED);

    static final private int _ERROR_CANCELLED = 31;

    static final public Result ERROR_CANCELLED = new Result(_ERROR_CANCELLED);

    static final private int _ERROR_DIRECTORY_BUSY = 32;

    static final public Result ERROR_DIRECTORY_BUSY = new Result(_ERROR_DIRECTORY_BUSY);

    static final private int _ERROR_DIRECTORY_NOT_EMPTY = 33;

    static final public Result ERROR_DIRECTORY_NOT_EMPTY = new Result(_ERROR_DIRECTORY_NOT_EMPTY);

    static final private int _ERROR_TOO_MANY_LINKS = 34;

    static final public Result ERROR_TOO_MANY_LINKS = new Result(_ERROR_TOO_MANY_LINKS);

    static final private int _ERROR_READ_ONLY_FILE_SYSTEM = 35;

    static final public Result ERROR_READ_ONLY_FILE_SYSTEM = new Result(_ERROR_READ_ONLY_FILE_SYSTEM);

    static final private int _ERROR_NOT_SAME_FILE_SYSTEM = 36;

    static final public Result ERROR_NOT_SAME_FILE_SYSTEM = new Result(_ERROR_NOT_SAME_FILE_SYSTEM);

    static final private int _ERROR_NAME_TOO_LONG = 37;

    static final public Result ERROR_NAME_TOO_LONG = new Result(_ERROR_NAME_TOO_LONG);

    static final private int _ERROR_SERVICE_NOT_AVAILABLE = 38;

    static final public Result ERROR_SERVICE_NOT_AVAILABLE = new Result(_ERROR_SERVICE_NOT_AVAILABLE);

    static final private int _ERROR_SERVICE_OBSOLETE = 39;

    static final public Result ERROR_SERVICE_OBSOLETE = new Result(_ERROR_SERVICE_OBSOLETE);

    static final private int _ERROR_PROTOCOL_ERROR = 40;

    static final public Result ERROR_PROTOCOL_ERROR = new Result(_ERROR_PROTOCOL_ERROR);

    static final private int _ERROR_NO_MASTER_BROWSER = 41;

    static final public Result ERROR_NO_MASTER_BROWSER = new Result(_ERROR_NO_MASTER_BROWSER);

    static final private int _ERROR_NO_DEFAULT = 42;

    static final public Result ERROR_NO_DEFAULT = new Result(_ERROR_NO_DEFAULT);

    static final private int _ERROR_NO_HANDLER = 43;

    static final public Result ERROR_NO_HANDLER = new Result(_ERROR_NO_HANDLER);

    static final private int _ERROR_PARSE = 44;

    static final public Result ERROR_PARSE = new Result(_ERROR_PARSE);

    static final private int _ERROR_LAUNCH = 45;

    static final public Result ERROR_LAUNCH = new Result(_ERROR_LAUNCH);

    static final private int _ERROR_TIMEOUT = 46;

    static final public Result ERROR_TIMEOUT = new Result(_ERROR_TIMEOUT);

    static final private int _ERROR_NAMESERVER = 47;

    static final public Result ERROR_NAMESERVER = new Result(_ERROR_NAMESERVER);

    static final private int _ERROR_LOCKED = 48;

    static final public Result ERROR_LOCKED = new Result(_ERROR_LOCKED);

    static final private int _ERROR_DEPRECATED_FUNCTION = 49;

    static final public Result ERROR_DEPRECATED_FUNCTION = new Result(_ERROR_DEPRECATED_FUNCTION);

    static final private Result[] theInterned = new Result[] {
            OK, ERROR_NOT_FOUND, ERROR_GENERIC, ERROR_INTERNAL, ERROR_BAD_PARAMETERS,
            ERROR_NOT_SUPPORTED, ERROR_IO, ERROR_CORRUPTED_DATA, ERROR_WRONG_FORMAT,
            ERROR_BAD_FILE, ERROR_TOO_BIG, ERROR_NO_SPACE, ERROR_READ_ONLY, ERROR_INVALID_URI,
            ERROR_NOT_OPEN, ERROR_INVALID_OPEN_MODE, ERROR_ACCESS_DENIED, ERROR_TOO_MANY_OPEN_FILES,
            ERROR_EOF, ERROR_NOT_A_DIRECTORY, ERROR_IN_PROGRESS, ERROR_INTERRUPTED, ERROR_FILE_EXISTS,
            ERROR_LOOP, ERROR_NOT_PERMITTED, ERROR_IS_DIRECTORY, ERROR_NO_MEMORY, ERROR_HOST_NOT_FOUND,
            ERROR_INVALID_HOST_NAME, ERROR_HOST_HAS_NO_ADDRESS, ERROR_LOGIN_FAILED, ERROR_CANCELLED,
            ERROR_DIRECTORY_BUSY, ERROR_DIRECTORY_NOT_EMPTY, ERROR_TOO_MANY_LINKS, 
            ERROR_READ_ONLY_FILE_SYSTEM, ERROR_NOT_SAME_FILE_SYSTEM, ERROR_NAME_TOO_LONG,
            ERROR_SERVICE_NOT_AVAILABLE, ERROR_SERVICE_OBSOLETE, ERROR_PROTOCOL_ERROR,
            ERROR_NO_MASTER_BROWSER, ERROR_NO_DEFAULT, ERROR_NO_HANDLER, ERROR_PARSE, ERROR_LAUNCH,
            ERROR_TIMEOUT, ERROR_NAMESERVER, ERROR_LOCKED, ERROR_DEPRECATED_FUNCTION
    };

    static private java.util.Hashtable theInternedExtras;

    static final private Result theSacrificialOne = new Result(
            0);

    static public Result intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        Result already = (Result) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new Result(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private Result(int value) {
        value_ = value;
    }

    public Result or(Result other) {
        return intern(value_ | other.value_);
    }

    public Result and(Result other) {
        return intern(value_ & other.value_);
    }

    public Result xor(Result other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(Result other) {
        return (value_ & other.value_) == other.value_;
    }
    
    public String getText() {
        return gnome_vfs_result_to_string(getValue());
    }
    

    native static final protected String gnome_vfs_result_to_string(int result);
    native static final protected int gnome_vfs_result_from_errno_code(int errno);

}