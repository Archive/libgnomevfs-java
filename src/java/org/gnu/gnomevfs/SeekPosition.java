/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevfs;

import org.gnu.glib.Enum;

/**
 * This is used to specify the start positon for seek operations
 */
public class SeekPosition extends Enum {

    static final private int _START = 0;

    static final public SeekPosition START = new SeekPosition(_START);

    static final private int _CURRENT = 1;

    static final public SeekPosition CURRENT = new SeekPosition(_CURRENT);

    static final private int _END = 2;

    static final public SeekPosition END = new SeekPosition(_END);

    static final private SeekPosition[] theInterned = new SeekPosition[] {
            START, CURRENT, END }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private SeekPosition theSacrificialOne = new SeekPosition(
            0);

    static public SeekPosition intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        SeekPosition already = (SeekPosition) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new SeekPosition(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private SeekPosition(int value) {
        value_ = value;
    }

    public SeekPosition or(SeekPosition other) {
        return intern(value_ | other.value_);
    }

    public SeekPosition and(SeekPosition other) {
        return intern(value_ & other.value_);
    }

    public SeekPosition xor(SeekPosition other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(SeekPosition other) {
        return (value_ & other.value_) == other.value_;
    }

}