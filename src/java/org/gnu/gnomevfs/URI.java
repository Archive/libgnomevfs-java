/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevfs;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 * An object that describes a URI element
 */
public class URI extends GObject {
	
    /**
     * Create a new VFSURI from the provided uri String.
     * @param uri A String representing the URI
     */
    public URI(String uri) {
		super(gnome_vfs_uri_new(uri));
	}
    
    /**
     * Create a new VFSURI from the provided relatedReference related to the 
     * base VFSURI provided.
     * @param base
     * @param relatedReference
     */
    public URI(URI base, String relatedReference) {
        super(gnome_vfs_uri_resolve_relative(base.getHandle(), relatedReference));
    }
	
	/**
	 * Create a VFSURI that is a duplicate to the provided URI.
	 * @param uriToDuplicate
	 */
    public URI(URI uriToDuplicate) {
	    super(gnome_vfs_uri_dup(uriToDuplicate.getHandle()));
	}
	

	URI(Handle handle) {
		super(handle);
	}
	
	/**
	 * Create a new VFSURI obtained by appending uriFragment to the
	 * current VFSURI.
	 * 
	 * @param uriFragment
	 */
	public URI appendString(String uriFragment) {
		return new URI(gnome_vfs_uri_append_string(getHandle(), uriFragment));
	}
	
	/**
	 * Create a new VFSURI obtained by appending path to the
	 * current VFSURI.
	 * 
	 * @param path
	 */
	public URI appendPath(String path) {
		return new URI(gnome_vfs_uri_append_path(getHandle(), path));
	}
	
	/**
	 * Create a new VFSURI obtained by appending filename to the
	 * current VFSURI.
	 * 
	 * @param filename
	 */
	public URI appendFilename(String filename) {
		return new URI(gnome_vfs_uri_append_file_name(getHandle(), filename));
	}
	
	/**
	 * Translate the VFSURI into a printable string.
	 * 
	 * @param hideOptions
	 */
	public String toString(URIHideOptions hideOptions) {
		return gnome_vfs_uri_to_string(getHandle(), hideOptions.getValue());
	}
	
	/**
	 * Check if uri is local (native) file system.
	 */
	public boolean isLocal() {
		return gnome_vfs_uri_is_local(getHandle());
	}
	
	/**
	 * Check if the uri has a parent or not.
	 */
	public boolean hasParent() {
		return gnome_vfs_uri_has_parent(getHandle());
	}
	
	/**
	 * Retrieve the uri's parent.
	 */
	public URI getParent() {
		return new URI(gnome_vfs_uri_get_parent(getHandle()));
	}
	
	/**
	 * Retrieve the toplevel VFSURI in the uri.
	 */
	public URI getToplevel() {
		return new URI(gnome_vfs_uri_get_toplevel(getHandle()));
	}
	
	/**
	 * Retrieve the host name for uri.
	 */
	public String getHostName() {
		return gnome_vfs_uri_get_host_name(getHandle());
	}
	
	/**
	 * Retrieve the scheme for uri.
	 */
	public String getScheme() {
		return gnome_vfs_uri_get_scheme(getHandle());
	}
	
	/**
	 * Retrieve the host port number in uri.
	 */
	public int getHostPort() {
		return gnome_vfs_uri_get_host_port(getHandle());
	}
	
	/**
	 * Retrieve the user name in uri.
	 */
	public String getUserName() {
		return gnome_vfs_uri_get_user_name(getHandle());
	}
	
	/**
	 * Retrieve the password in uri.
	 */
	public String getPassword() {
		return gnome_vfs_uri_get_password(getHandle());
	}
	
	/**
	 * Set the host name accessed by uri.
	 * 
	 * @param hostname
	 */
	public void setHostName(String hostname) {
		gnome_vfs_uri_set_host_name(getHandle(), hostname);
	}
	
	/**
	 * Set the host port number in uri.  If the port number
	 * is zero, the default port for the uri's toplevel access
	 * method is used.
	 * 
	 * @param portNumber
	 */
	public void setHostPort(int portNumber) {
		gnome_vfs_uri_set_host_port(getHandle(), portNumber);
	}
	
	/**
	 * Set the user name for the uri.
	 * 
	 * @param username
	 */
	public void setUserName(String username) {
		gnome_vfs_uri_set_user_name(getHandle(), username);
	}
	
	/**
	 * Set the password for the uri.
	 * 
	 * @param password
	 */
	public void setPassword(String password) {
		gnome_vfs_uri_set_password(getHandle(), password);
	}
	
	/**
	 * Returns true if the provided uri is equal to this object.
	 * 
	 * @param uri
	 */
	public boolean isEqual(URI uri) {
		return gnome_vfs_uri_equal(getHandle(), uri.getHandle());
	}
	
	/**
	 * Check if child is contained in this uri.
	 * 
	 * @param child
	 * @param recursive
	 */
	public boolean isParent(URI child, boolean recursive) {
		return gnome_vfs_uri_is_parent(getHandle(), child.getHandle(), recursive);
	}
	
	/**
	 * Retrieve the full path name for the uri.
	 */
	public String getPath() {
		return gnome_vfs_uri_get_path(getHandle());
	}

	public String getFragmentIdentifier() {
	    return gnome_vfs_uri_get_fragment_identifier(getHandle());
	}

	/**
	 * Extracts the name of the directory in which the file pointed to
	 * by this VFSURI is stored.  
	 */
	public String extractDirectoryName() {
	    return gnome_vfs_uri_extract_dirname(getHandle());
	}
	
	/**
	 * Retrieve the base file name for this VFSURI ignoring any
	 * trailing path separators.  This is often useful when you
	 * want the name of something that's pointed to and don't care
	 * whether the uri has a directory or file form.
	 */
	public String extractShortName() {
	    return gnome_vfs_uri_extract_short_name(getHandle());
	}
	
	/**
	 * Retrieve the base file name for this VFSURI ignoring any
	 * trailing path separators.  This is often useful when you
	 * want the name of something that's pointed to and don't care
	 * whether the uri has a directory or file form.
	 */
	public String extractShortPathName() {
	    return gnome_vfs_uri_extract_short_path_name(getHandle());
	}
	
	/**
	 * Returns the full URI given the full base URI and a secondary
	 * URI which may be relative.
	 * @param baseURI
	 * @param relativeURI
	 */
	public String makeFullFromRelative(String baseURI, String relativeURI) {
	    return gnome_vfs_uri_make_full_from_relative(getHandle(), baseURI, relativeURI);
	}


    native static final protected Handle gnome_vfs_uri_new(String uri);
    native static final protected Handle gnome_vfs_uri_resolve_relative(Handle base, String relatedReference);
    native static final protected Handle gnome_vfs_uri_append_string(Handle uri, String uriFragment);
    native static final protected Handle gnome_vfs_uri_append_path(Handle uri, String path);
    native static final protected Handle gnome_vfs_uri_append_file_name(Handle uri, String filename);
    native static final protected String gnome_vfs_uri_to_string(Handle uri, int hideOptions);
    native static final protected Handle gnome_vfs_uri_dup(Handle uri);
    native static final protected boolean gnome_vfs_uri_is_local(Handle uri);
    native static final protected boolean gnome_vfs_uri_has_parent(Handle uri);
    native static final protected Handle gnome_vfs_uri_get_parent(Handle uri);
    native static final protected Handle gnome_vfs_uri_get_toplevel(Handle uri);
    native static final protected String gnome_vfs_uri_get_host_name(Handle uri);
    native static final protected String gnome_vfs_uri_get_scheme(Handle uri);
    native static final protected int gnome_vfs_uri_get_host_port(Handle uri);
    native static final protected String gnome_vfs_uri_get_user_name(Handle uri);
    native static final protected String gnome_vfs_uri_get_password(Handle uri);
    native static final protected void gnome_vfs_uri_set_host_name(Handle uri, String host);
    native static final protected void gnome_vfs_uri_set_host_port(Handle uri, int port);
    native static final protected void gnome_vfs_uri_set_user_name(Handle uri, String username);
    native static final protected void gnome_vfs_uri_set_password(Handle uri, String password);
    native static final protected boolean gnome_vfs_uri_equal(Handle a, Handle b);
    native static final protected boolean gnome_vfs_uri_is_parent(Handle uri, Handle child, boolean recursive);
    native static final protected String gnome_vfs_uri_get_path(Handle uri);
    native static final protected String gnome_vfs_uri_get_fragment_identifier(Handle uri);
    native static final protected String gnome_vfs_uri_extract_dirname(Handle uri);
    native static final protected String gnome_vfs_uri_extract_short_name(Handle uri);
    native static final protected String gnome_vfs_uri_extract_short_path_name(Handle uri);
    native static final protected String gnome_vfs_uri_make_full_from_relative(Handle uri, String base, String relative);

}
