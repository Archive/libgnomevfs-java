/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevfs;

import org.gnu.glib.Flags;

/**
 * Controls hiding of various elements of a VFSURI when
 * it is converted to a String.
 */
public class URIHideOptions extends Flags {
    static final private int _NONE = 0;
    /** Don't hide anything */
    static final public URIHideOptions NONE = new URIHideOptions(_NONE);

    static final private int _USER_NAME = 1 << 0;
    /** Hide the user name */
    static final public URIHideOptions USER_NAME = new URIHideOptions(_USER_NAME);

    static final private int _PASSWORD = 1 << 1;
    /** Hide the password */
    static final public URIHideOptions PASSWORD = new URIHideOptions(_PASSWORD);

    static final private int _HOST_NAME = 1 << 2;
    /** Hide the hostname */
    static final public URIHideOptions HOST_NAME = new URIHideOptions(_HOST_NAME);

    static final private int _HOST_PORT = 1 << 3;
    /** Hide the port */
    static final public URIHideOptions HOST_PORT = new URIHideOptions(_HOST_PORT);

    static final private int _TOPLEVEL_METHOD = 1 << 4;
    /** Hide the method */
    static final public URIHideOptions TOPLEVEL_METHOD = new URIHideOptions(_TOPLEVEL_METHOD);

    static final private int _FRAGMENT_IDENTIFIER = 1 << 8;
    /** Hide the fragment identifier */
    static final public URIHideOptions FRAGMENT_IDENTIFIER = new URIHideOptions(_FRAGMENT_IDENTIFIER);


    
    static final private URIHideOptions[] theInterned = new URIHideOptions[] { NONE,
    		PASSWORD, HOST_NAME, HOST_PORT, TOPLEVEL_METHOD, FRAGMENT_IDENTIFIER };

    static private java.util.Hashtable theInternedExtras;

    static final private URIHideOptions theSacrificialOne = new URIHideOptions(0);

    static public URIHideOptions intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        URIHideOptions already = (URIHideOptions) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new URIHideOptions(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private URIHideOptions(int value) {
        value_ = value;
    }

    public URIHideOptions or(URIHideOptions other) {
        return intern(value_ | other.value_);
    }

    public URIHideOptions and(URIHideOptions other) {
        return intern(value_ & other.value_);
    }

    public URIHideOptions xor(URIHideOptions other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(URIHideOptions other) {
        return (value_ & other.value_) == other.value_;
    }
}
