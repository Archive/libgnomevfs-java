/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevfs;

import org.gnu.glib.Handle;


/**
 * Various utility methods.
 */
public class Utils {

    /**
     * Formats the file size passed in <i>bytes</i> in a way that is
     * easy for the user to read.  Gives the size in bytes, kilobytes,
     * megabytes, or gigabytes, choosing whatever is appropriate.
     * @param filesize The filesize in bytes.
     */
    static public String formatFileSizeForDisplay(long filesize) {
        return gnome_vfs_format_file_size_for_display(filesize);
    }
    
    /**
     * Filter, modify, unescape and change the URI to make them
     * appropriate to display to users.  
     * @param uri
     */
    static public String formatURIForDisplay(String uri) {
        return gnome_vfs_format_uri_for_display(uri);
    }
    
    /**
     * Launches the default application or component associated with
     * the given url.
     * @param url
     */
    static public Result showURL(String url) {
        return Result.intern(gnome_vfs_url_show(url));
    }
    
    /**
     * Like showURL except that the default application will be launched
     * with the given environment.
     * @param url
     * @param environment
     */
    static public Result showURLWithEnvironment(String url, String[] environment) {
        return Result.intern(gnome_vfs_url_show_with_env(url, environment));
    }
    
    /**
     * Escapes a String replacing any and all special characters with
     * the equivalent escape sequence.
     * @param str
     */
    static public String escapeString(String str) {
        return gnome_vfs_escape_string(str);
    }
    
    /**
     * Escapes a path replacing only special characters that would
     * not be found in paths so '/', '&', '=', and '?' will not
     * be escaped by this method.
     * @param path
     */
    static public String escapePath(String path) {
        return gnome_vfs_escape_path_string(path);
    }
    
    /**
     * Escapes a path replacing only special characters that would
     * not be found in paths or host names so '/', '&', '=', ':',
     * '@', and '?' will not be escaped by this method.
     * @param path
     */
    static public String escapeHostAndPath(String path) {
        return gnome_vfs_escape_host_and_path_string(path);
    }
    
    /**
     * Escape only '/' and '%' characters in String replacing them
     * with their escape sequence equivalents.
     * @param str
     */
    static public String escapeSlashes(String str) {
        return gnome_vfs_escape_slashes(str);
    }
    
    static public String escapeSet(String str, String matchSet) {
        return gnome_vfs_escape_set(str, matchSet);
    }
    
    /**
     * Decodes escaped characters (i.e. PERCENTxx sequences) in the provided
     * String.  Characters are encoded in PERCENTxy form, where xy is the
     * ASCII hex code for character 16x+y.
     * @param escapedStr An escaped URI, path, or other String.
     * @param illegalChars A string containing a sequence of characters that
     * are considered illegal.  
     */
    static public String unescapeString(String escapedStr, String illegalChars) {
        return gnome_vfs_unescape_string(escapedStr, illegalChars);
    }
    
    /**
     * Standardizes the format of the URI being passed so that it can be
     * used later in other methods that accept a canonical URI. If stripFragment
     * is true this method strips anything after the '#' prior to making it
     * canonical.
     * @param uri
     * @param stripFragment
     */
    static public String makeURICanonical(String uri, boolean stripFragment) {
        if (stripFragment)
            return gnome_vfs_make_uri_canonical_strip_fragment(uri);
        return gnome_vfs_make_uri_canonical(uri);
    }
    
    /**
     * Takes a user input path/URI and makes a valid URI out of it.
     * This method is the reverse of formatURIForDisplay.
     * @param uri
     */
    static public String makeURIFromInput(String uri) {
        return gnome_vfs_make_uri_from_input(uri);
    }
    
    /**
     * Similar to makeURIFromInput except that it guesses relative
     * paths instead of http domains, it doesn't bother stripping
     * leading/trailing white space, and it doesn't bother with
     * ~ expansion - that's done by the shell.
     * @param uri
     */
    static public String makeURIFromShellArg(String uri) {
        return gnome_vfs_make_uri_from_shell_arg(uri);
    }
    
    /**
     * If the path starts with a '~', representing a users home directory,
     * expand it to the actual path location.
     * @param path
     */
    static public String expandInitialTilde(String path) {
        return gnome_vfs_expand_initial_tilde(path);
    }
    
    /**
     * 
     * @param escaped
     */
    static public String unescapeStringForDisplay(String escaped) {
        return gnome_vfs_unescape_string_for_display(escaped);
    }
    
    /**
     * Create a local path for a file:/// URI.  Do not use with
     * URIs of other methods.
     * @param uri
     */
    static public String getLocalPathFromURI(String uri) {
        return gnome_vfs_get_local_path_from_uri(uri);
    }
    
    /**
     * Returns a file:/// URI for the local path provided.
     * @param fullLocalPath
     */
    static public String getURIFromLocalPath(String fullLocalPath) {
        return gnome_vfs_get_uri_from_local_path(fullLocalPath);
    }
    
    /**
     * Checks if the provided command string starts with the full
     * path of an executable file or an executable in $PATH.
     * @param command
     */
    static boolean isExecutableCommandString(String command) {
        return gnome_vfs_is_executable_command_string(command);
    }
    
    /**
     * Returns the amount of free space on a volume.  This only works
     * for URIs with the file: scheme.
     * @param uri
     * @throws VFSException
     */
    static long getVolumeFreeSpace(URI uri) throws VFSException {
        long[] size = new long[1];
        Result result = Result.intern(gnome_vfs_get_volume_free_space(uri.getHandle(), size));
		if (result != Result.OK)
			throw new VFSException(result);
		return size[0];
    }
    
    static public String iconPathFromFilename(String filename) {
        return gnome_vfs_icon_path_from_filename(filename);
    }
    
    /**
     * Retrieve the scheme used in the provided uri.
     * @param uri
     */
    static public String getURIScheme(String uri) {
        return gnome_vfs_get_uri_scheme(uri);
    }
    
    /**
     * Compare two URIs.
     * @param uri1
     * @param uri2
     */
    static public boolean urisMatch(String uri1, String uri2) {
        return gnome_vfs_uris_match(uri1, uri2);
    }
    
    /**
     * Reads an entire file and returns a String containing the
     * contents.  Beware of accidentlly loading large files into
     * memory with this method. 
     * @param uri
     * @throws VFSException
     */
    static public String readEntireFile(String uri) throws VFSException {
        long[] size = new long[1];
        String contents = new String();
        Result result = Result.intern(gnome_vfs_read_entire_file(uri, size, contents));
		if (result != Result.OK)
			throw new VFSException(result);
		return contents;
    }
    
    native static final protected String gnome_vfs_format_file_size_for_display(long fileSize);
    native static final protected String gnome_vfs_format_uri_for_display(String uri);
    native static final protected int gnome_vfs_url_show(String url);
    native static final protected int gnome_vfs_url_show_with_env(String url, String[] env);
    native static final protected String gnome_vfs_escape_string(String str);
    native static final protected String gnome_vfs_escape_path_string(String path);
    native static final protected String gnome_vfs_escape_host_and_path_string(String path);
    native static final protected String gnome_vfs_escape_slashes(String str);
    native static final protected String gnome_vfs_escape_set(String str, String matchSet);
    native static final protected String gnome_vfs_unescape_string(String escapedStr, String illegalChars);
    native static final protected String gnome_vfs_make_uri_canonical(String uri);
    native static final protected String gnome_vfs_make_uri_canonical_strip_fragment(String uri);
    native static final protected String gnome_vfs_make_uri_from_input(String uri);
    native static final protected String gnome_vfs_make_uri_from_shell_arg(String uri);
    native static final protected String gnome_vfs_expand_initial_tilde(String path);
    native static final protected String gnome_vfs_unescape_string_for_display(String escaped);
    native static final protected String gnome_vfs_get_local_path_from_uri(String uri);
    native static final protected String gnome_vfs_get_uri_from_local_path(String fullLocalPath);
    native static final protected boolean gnome_vfs_is_executable_command_string(String commandString);
    native static final protected int gnome_vfs_get_volume_free_space(Handle uri, long[] size);
    native static final protected String gnome_vfs_icon_path_from_filename(String filename);
    native static final protected String gnome_vfs_get_uri_scheme(String uri);
    native static final protected boolean gnome_vfs_uris_match(String uri1, String uri2);
    native static final protected int gnome_vfs_read_entire_file(String uri, long[] filesize, String contents);

}
