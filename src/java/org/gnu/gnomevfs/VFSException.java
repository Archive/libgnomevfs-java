/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevfs;

/**
 */
public class VFSException extends Exception {
	
	Result result;
	
	public VFSException(Result result) {
		this.result = result;
	}
	
	private VFSException() {}
	
	public Result getError() {
		return result;
	}

}
