/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevfs;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 */
public class Volume extends GObject {
	
	Volume(Handle hndl) {
		super(hndl);
	}

	public long getID() {
		return gnome_vfs_volume_get_id(getHandle());
	}

	public VolumeType getVolumeType() {
		return VolumeType.intern(gnome_vfs_volume_get_volume_type(getHandle()));
	}
	
	public DeviceType getDeviceType() {
		return DeviceType.intern(gnome_vfs_volume_get_device_type(getHandle()));
	}

	public Drive getDrive() {
		return new Drive(gnome_vfs_volume_get_drive(getHandle()));
	}

	public String getFilesystemType() {
		return gnome_vfs_volume_get_filesystem_type(getHandle());
	}

	public String getDevicePath() {
		return gnome_vfs_volume_get_device_path(getHandle());
	}

	public String getActivationURI() {
		return gnome_vfs_volume_get_activation_uri(getHandle());
	}

	public String getDisplayName() {
		return gnome_vfs_volume_get_display_name(getHandle());
	}

	public String getIcon() {
		return gnome_vfs_volume_get_icon(getHandle());
	}

	public String getHalUdi() {
		return gnome_vfs_volume_get_hal_udi(getHandle());
	}

	public boolean isUserVisible() {
		return gnome_vfs_volume_is_user_visible(getHandle());
	}

	public boolean isMounted() {
		return gnome_vfs_volume_is_mounted(getHandle());
	}
	
	public boolean isReadOnly() {
		return gnome_vfs_volume_is_read_only(getHandle());
	}
	
	public boolean handlesTrash() {
		return gnome_vfs_volume_handles_trash(getHandle());
	}

	static public int compare(Volume a, Volume b) {
		return gnome_vfs_volume_compare(a.getHandle(), b.getHandle());
	}
	
	/*
	 * gnome_vfs_volume_unmount callback method
	 */
	private void unmount(boolean success, String error, String detailError) {
	
	}
	
	/*
	 * gnome_vfs_volume_eject callback method
	 */
	private void eject(boolean success, String error, String detailError) {
		
	}
	
	native static final private int gnome_vfs_volume_get_type();
	native static final private Handle gnome_vfs_volume_ref(Handle vol);
	native static final private void gnome_vfs_volume_unref(Handle vol);
	native static final private long gnome_vfs_volume_get_id(Handle vol);
	native static final private int gnome_vfs_volume_get_volume_type(Handle vol);
	native static final private int gnome_vfs_volume_get_device_type(Handle vol);
	native static final private Handle gnome_vfs_volume_get_drive(Handle vol);
	native static final private String gnome_vfs_volume_get_device_path(Handle vol);
	native static final private String gnome_vfs_volume_get_activation_uri(Handle vol);
	native static final private String gnome_vfs_volume_get_filesystem_type(Handle vol);
	native static final private String gnome_vfs_volume_get_display_name(Handle vol);
	native static final private String gnome_vfs_volume_get_icon(Handle vol);
	native static final private String gnome_vfs_volume_get_hal_udi(Handle vol);
	native static final private boolean gnome_vfs_volume_is_user_visible(Handle vol);
	native static final private boolean gnome_vfs_volume_is_read_only(Handle vol);
	native static final private boolean gnome_vfs_volume_is_mounted(Handle vol);
	native static final private boolean gnome_vfs_volume_handles_trash(Handle vol);
	native static final private int gnome_vfs_volume_compare(Handle a, Handle b);
	
}
