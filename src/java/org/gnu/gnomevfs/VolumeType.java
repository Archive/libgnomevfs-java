/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gnomevfs;

import org.gnu.glib.Enum;

/**
 */
public class VolumeType extends Enum {
    static final private int _MOUNTPOINT = 0;
    static final public VolumeType MOUNTPOINT = new VolumeType(_MOUNTPOINT);

    static final private int _VFS_MOUNT = 1;
    static final public VolumeType VFS_MOUNT = new VolumeType(_VFS_MOUNT);
    
    static final private int _CONNECTED_SERVER = 2;
    static final public VolumeType CONNECTED_SERVER = new VolumeType(_CONNECTED_SERVER);

    static final private VolumeType[] theInterned = new VolumeType[] {
    	MOUNTPOINT, VFS_MOUNT, CONNECTED_SERVER
    };

    static private java.util.Hashtable theInternedExtras;

    static final private VolumeType theSacrificialOne = new VolumeType(0);

    static public VolumeType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        VolumeType already = (VolumeType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new VolumeType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private VolumeType(int value) {
        value_ = value;
    }

    public boolean test(VolumeType other) {
        return (value_ & other.value_) == other.value_;
    }
}
