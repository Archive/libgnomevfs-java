/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <libgnomevfs/gnome-vfs.h>
#include <jg_jnu.h>

#ifndef _Included_org_gnu_gnomevfs_Directory
#define _Included_org_gnu_gnomevfs_Directory
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gnomevfs_Directory
 * Method:    gnome_vfs_directory_open
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_Directory_gnome_1vfs_1directory_1open
  (JNIEnv *env, jclass cls, jobject handle, jstring uri, jint options)
{
	const gchar* str = (*env)->GetStringUTFChars(env, uri, NULL);
	GnomeVFSDirectoryHandle *h;
	GnomeVFSResult result = gnome_vfs_directory_open(&h, str, (GnomeVFSFileInfoOptions)options);
	if (result == GNOME_VFS_OK)
		updateHandle(env, handle, h);
	(*env)->ReleaseStringUTFChars(env, uri, str);
	return result;
}

/*
 * Class:     org_gnu_gnomevfs_Directory
 * Method:    gnome_vfs_directory_open_from_uri
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_Directory_gnome_1vfs_1directory_1open_1from_1uri
  (JNIEnv *env, jclass cls, jobject handle, jobject uri, jint options)
{
	GnomeVFSDirectoryHandle *h;
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	GnomeVFSResult result = gnome_vfs_directory_open_from_uri(&h, uri_g, (GnomeVFSFileInfoOptions)options);
	if (result == GNOME_VFS_OK)
		updateHandle(env, handle, h);
	return result;
}

/*
 * Class:     org_gnu_gnomevfs_Directory
 * Method:    gnome_vfs_directory_read_next
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_Directory_gnome_1vfs_1directory_1read_1next
  (JNIEnv *env, jclass cls, jobject dir, jobject info)
{
	GnomeVFSDirectoryHandle* dir_g = (GnomeVFSDirectoryHandle*)getPointerFromHandle(env, dir);
	GnomeVFSFileInfo *i = NULL;
	GnomeVFSResult result = gnome_vfs_directory_read_next(dir_g, i);
	if (result == GNOME_VFS_OK)
		updateHandle(env, info, i);
	return result;
	
}

/*
 * Class:     org_gnu_gnomevfs_Directory
 * Method:    gnome_vfs_directory_close
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_Directory_gnome_1vfs_1directory_1close
  (JNIEnv *env, jclass cls, jobject dir)
{
	GnomeVFSDirectoryHandle* dir_g = (GnomeVFSDirectoryHandle*)getPointerFromhandle(env, dir);
	return (jint)gnome_vfs_directory_close(dir_g);
}

/*
 * Class:     org_gnu_gnomevfs_Directory
 * Method:    gnome_vfs_make_directory
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_Directory_gnome_1vfs_1make_1directory
  (JNIEnv *env, jclass cls, jstring uri, jint perm)
{
	const gchar* str = (*env)->GetStringUTFChars(env, uri, NULL);
	jint result = (jint)gnome_vfs_make_directory(str, (guint)perm);
	(*env)->ReleaseStringUTFChars(env, uri, str);
	return result;
}

/*
 * Class:     org_gnu_gnomevfs_Directory
 * Method:    gnome_vfs_make_directory_for_uri
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_Directory_gnome_1vfs_1make_1directory_1for_1uri
  (JNIEnv *env, jclass cls, jobject uri, jint perm)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	return (jint)gnome_vfs_make_directory_for_uri(uri_g, (guint)perm);
}

/*
 * Class:     org_gnu_gnomevfs_Directory
 * Method:    gnome_vfs_remove_directory
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_Directory_gnome_1vfs_1remove_1directory
  (JNIEnv *env, jclass cls, jstring uri)
{
	const gchar* str = (*env)->GetStringUTFChars(env, uri, NULL);
	jint result = (jint)gnome_vfs_remove_directory(str);
	(*env)->ReleaseStringUTFChars(env, uri, str);
	return result;
}

/*
 * Class:     org_gnu_gnomevfs_Directory
 * Method:    gnome_vfs_remove_directory_from_uri
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_Directory_gnome_1vfs_1remove_1directory_1from_1uri
  (JNIEnv *env, jclass cls, jobject uri)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	return (jint)gnome_vfs_remove_directory_from_uri(uri_g);
}

/*
 * Class:     org_gnu_gnomevfs_Directory
 * Method:    gnome_vfs_find_directory
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_Directory_gnome_1vfs_1find_1directory
  (JNIEnv *env, jclass cls, jobject near, jint kind, jobject hndl, 
  		jboolean createIfNeeded, jboolean findIfNeeded, jint perms)
{
	GnomeVFSURI *h;
	GnomeVFSURI* near_g = (GnomeVFSURI*)getPointerFromHandle(env, near);
	GnomeVFSResult result = gnome_vfs_find_directory(near_g, (GnomeVFSFindDirectoryKind)kind,
			&h, createIfNeeded, findIfNeeded, perms);
	if (result == GNOME_VFS_OK)
		updateHandle(env, hndl, h);
	return result;
}



#ifdef __cplusplus
}
#endif
#endif
