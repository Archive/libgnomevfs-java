/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <libgnomevfs/gnome-vfs.h>
#include <jg_jnu.h>

#ifndef _Included_org_gnu_gnomevfs_Drive
#define _Included_org_gnu_gnomevfs_Drive
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gnomevfs_Drive
 * Method:    gnome_vfs_drive_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_Drive_gnome_1vfs_1drive_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gnome_vfs_drive_get_type();
}

/*
 * Class:     org_gnu_gnomevfs_Drive
 * Method:    gnome_vfs_drive_ref
 * Signature: (Lorg/gnu/glib/Handle;)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnomevfs_Drive_gnome_1vfs_1drive_1ref
  (JNIEnv *env, jclass cls, jobject drive)
{
	GnomeVFSDrive* drive_g = (GnomeVFSDrive*)getPointerFromHandle(env, drive);
	return getHandleFromPointer(env, gnome_vfs_drive_ref(drive_g));
}

/*
 * Class:     org_gnu_gnomevfs_Drive
 * Method:    gnome_vfs_drive_unref
 * Signature: (Lorg/gnu/glib/Handle;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevfs_Drive_gnome_1vfs_1drive_1unref
  (JNIEnv *env, jclass cls, jobject drive)
{
	GnomeVFSDrive* drive_g = (GnomeVFSDrive*)getPointerFromHandle(env, drive);
	gnome_vfs_drive_unref(drive_g);
}

/*
 * Class:     org_gnu_gnomevfs_Drive
 * Method:    gnome_vfs_drive_volume_list_free
 * Signature: (Lorg/gnu/glib/Handle;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevfs_Drive_gnome_1vfs_1drive_1volume_1list_1free
  (JNIEnv *env, jclass cls, jobjectArray drive)
{
	GList *list = getGListFromHandles(env, drive);
	gnome_vfs_drive_volume_list_free(list);
}

/*
 * Class:     org_gnu_gnomevfs_Drive
 * Method:    gnome_vfs_drive_get_id
 * Signature: (Lorg/gnu/glib/Handle;)J
 */
JNIEXPORT jlong JNICALL Java_org_gnu_gnomevfs_Drive_gnome_1vfs_1drive_1get_1id
  (JNIEnv *env, jclass cls, jobject drive)
{
	GnomeVFSDrive* drive_g = (GnomeVFSDrive*)getPointerFromHandle(env, drive);
	return (jlong)gnome_vfs_drive_get_id(drive_g);
}

/*
 * Class:     org_gnu_gnomevfs_Drive
 * Method:    gnome_vfs_drive_get_device_type
 * Signature: (Lorg/gnu/glib/Handle;)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_Drive_gnome_1vfs_1drive_1get_1device_1type
  (JNIEnv *env, jclass cls, jobject drive)
{
	GnomeVFSDrive* drive_g = (GnomeVFSDrive*)getPointerFromHandle(env, drive);
	return (jint)gnome_vfs_drive_get_device_type(drive_g);
}

/*
 * Class:     org_gnu_gnomevfs_Drive
 * Method:    gnome_vfs_drive_get_mounted_volumes
 * Signature: (Lorg/gnu/glib/Handle;)[Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gnomevfs_Drive_gnome_1vfs_1drive_1get_1mounted_1volumes
  (JNIEnv *env, jclass cls, jobject drive)
{
	GnomeVFSDrive* drive_g = (GnomeVFSDrive*)getPointerFromHandle(env, drive);
	return getHandleArrayFromGList(env, gnome_vfs_drive_get_mounted_volumes(drive_g));
}

/*
 * Class:     org_gnu_gnomevfs_Drive
 * Method:    gnome_vfs_drive_get_device_path
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Drive_gnome_1vfs_1drive_1get_1device_1path
  (JNIEnv *env, jclass cls, jobject drive)
{
	GnomeVFSDrive* drive_g = (GnomeVFSDrive*)getPointerFromHandle(env, drive);
	return (*env)->NewStringUTF(env, gnome_vfs_drive_get_device_path(drive_g));
}

/*
 * Class:     org_gnu_gnomevfs_Drive
 * Method:    gnome_vfs_drive_get_activation_uri
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Drive_gnome_1vfs_1drive_1get_1activation_1uri
  (JNIEnv *env, jclass cls, jobject drive)
{
	GnomeVFSDrive* drive_g = (GnomeVFSDrive*)getPointerFromHandle(env, drive);
	return (*env)->NewStringUTF(env, gnome_vfs_drive_get_activation_uri(drive_g));
}

/*
 * Class:     org_gnu_gnomevfs_Drive
 * Method:    gnome_vfs_drive_get_display_name
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Drive_gnome_1vfs_1drive_1get_1display_1name
  (JNIEnv *env, jclass cls, jobject drive)
{
	GnomeVFSDrive* drive_g = (GnomeVFSDrive*)getPointerFromHandle(env, drive);
	return (*env)->NewStringUTF(env, gnome_vfs_drive_get_display_name(drive_g));
}

/*
 * Class:     org_gnu_gnomevfs_Drive
 * Method:    gnome_vfs_drive_get_icon
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Drive_gnome_1vfs_1drive_1get_1icon
  (JNIEnv *env, jclass cls, jobject drive)
{
	GnomeVFSDrive* drive_g = (GnomeVFSDrive*)getPointerFromHandle(env, drive);
	return (*env)->NewStringUTF(env, gnome_vfs_drive_get_icon(drive_g));
}

/*
 * Class:     org_gnu_gnomevfs_Drive
 * Method:    gnome_vfs_drive_get_hal_udi
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Drive_gnome_1vfs_1drive_1get_1hal_1udi
  (JNIEnv *env, jclass cls, jobject drive)
{
	GnomeVFSDrive* drive_g = (GnomeVFSDrive*)getPointerFromHandle(env, drive);
	return (*env)->NewStringUTF(env, gnome_vfs_drive_get_hal_udi(drive_g));
}

/*
 * Class:     org_gnu_gnomevfs_Drive
 * Method:    gnome_vfs_drive_is_user_visible
 * Signature: (Lorg/gnu/glib/Handle;)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevfs_Drive_gnome_1vfs_1drive_1is_1user_1visible
  (JNIEnv *env, jclass cls, jobject drive)
{
	GnomeVFSDrive* drive_g = (GnomeVFSDrive*)getPointerFromHandle(env, drive);
	return (jboolean)gnome_vfs_drive_is_user_visible(drive_g);
}

/*
 * Class:     org_gnu_gnomevfs_Drive
 * Method:    gnome_vfs_drive_is_connected
 * Signature: (Lorg/gnu/glib/Handle;)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevfs_Drive_gnome_1vfs_1drive_1is_1connected
  (JNIEnv *env, jclass cls, jobject drive)
{
	GnomeVFSDrive* drive_g = (GnomeVFSDrive*)getPointerFromHandle(env, drive);
	return (jboolean)gnome_vfs_drive_is_connected(drive_g);
}

/*
 * Class:     org_gnu_gnomevfs_Drive
 * Method:    gnome_vfs_drive_is_mounted
 * Signature: (Lorg/gnu/glib/Handle;)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevfs_Drive_gnome_1vfs_1drive_1is_1mounted
  (JNIEnv *env, jclass cls, jobject drive)
{
	GnomeVFSDrive* drive_g = (GnomeVFSDrive*)getPointerFromHandle(env, drive);
	return (jboolean)gnome_vfs_drive_is_mounted(drive_g);
}

/*
 * Class:     org_gnu_gnomevfs_Drive
 * Method:    gnome_vfs_drive_compare
 * Signature: (Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_Drive_gnome_1vfs_1drive_1compare
  (JNIEnv *env, jclass cls, jobject drivea, jobject driveb)
{
	GnomeVFSDrive* drivea_g = (GnomeVFSDrive*)getPointerFromHandle(env, drivea);
	GnomeVFSDrive* driveb_g = (GnomeVFSDrive*)getPointerFromHandle(env, driveb);
	return gnome_vfs_drive_compare(drivea_g, driveb_g);
}

#ifdef __cplusplus
}
#endif
#endif
