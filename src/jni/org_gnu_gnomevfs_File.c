/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <libgnomevfs/gnome-vfs.h>
#include <jg_jnu.h>

#ifndef _Included_org_gnu_gnomevfs_File
#define _Included_org_gnu_gnomevfs_File
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gnomevfs_File
 * Method:    gnome_vfs_create
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_File_gnome_1vfs_1create
  (JNIEnv *env, jclass cls, jobject handle, jstring uri, jint openMode, jboolean exclusive, jint perm)
{
	const gchar* str = (*env)->GetStringUTFChars(env, uri, NULL);
	GnomeVFSHandle *h;
	GnomeVFSResult result = gnome_vfs_create(&h, str, (GnomeVFSOpenMode)openMode, exclusive, perm);
	if (result == GNOME_VFS_OK)
		updateHandle(env, handle, h);
	(*env)->ReleaseStringUTFChars(env, uri, str);
	return result;
}

/*
 * Class:     org_gnu_gnomevfs_File
 * Method:    gnome_vfs_create_uri
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_File_gnome_1vfs_1create_1uri
  (JNIEnv *env, jclass cls, jobject handle, jobject uri, jint openMode, jboolean exclusive, jint perm)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	GnomeVFSHandle *h;
	GnomeVFSResult result = gnome_vfs_create_uri(&h, uri_g, (GnomeVFSOpenMode)openMode, exclusive, perm);
	if (result == GNOME_VFS_OK)
		updateHandle(env, handle, h);
	return result;
}

/*
 * Class:     org_gnu_gnomevfs_File
 * Method:    gnome_vfs_open
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_File_gnome_1vfs_1open
  (JNIEnv *env, jclass cls, jobject handle, jstring uri, jint openMode)
{
	const gchar* str = (*env)->GetStringUTFChars(env, uri, NULL);
	GnomeVFSHandle *h;
	GnomeVFSResult result = gnome_vfs_open(&h, str, (GnomeVFSOpenMode)openMode);
	if (result == GNOME_VFS_OK)
		updateHandle(env, handle, h);
	(*env)->ReleaseStringUTFChars(env, uri, str);
	return result;
}

/*
 * Class:     org_gnu_gnomevfs_File
 * Method:    gnome_vfs_open_uri
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_File_gnome_1vfs_1open_1uri
  (JNIEnv *env, jclass cls, jobject handle, jobject uri, jint openMode)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	GnomeVFSHandle *h;
	GnomeVFSResult result = gnome_vfs_open_uri(&h, uri_g, (GnomeVFSOpenMode)openMode);
	if (result == GNOME_VFS_OK)
		updateHandle(env, handle, h);
	return result;
}

/*
 * Class:     org_gnu_gnomevfs_File
 * Method:    gnome_vfs_close
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_File_gnome_1vfs_1close
  (JNIEnv *env, jclass cls, jobject handle)
{
	GnomeVFSHandle* handle_g = (GnomeVFSHandle*)getPointerFromHandle(env, handle);
	return (jint)gnome_vfs_close(handle_g);
}

/*
 * Class:     org_gnu_gnomevfs_File
 * Method:    gnome_vfs_unlink
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_File_gnome_1vfs_1unlink
  (JNIEnv *env, jclass cls, jstring uri)
{
	const gchar* str = (*env)->GetStringUTFChars(env, uri, NULL);
	jint retval = (jint)gnome_vfs_unlink(str);
	(*env)->ReleaseStringUTFChars(env, uri, str);
	return retval;
}

/*
 * Class:     org_gnu_gnomevfs_File
 * Method:    gnome_vfs_unlink_from_uri
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_File_gnome_1vfs_1unlink_1from_1uri
  (JNIEnv *env, jclass cls, jobject uri)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	return 	(jint)gnome_vfs_unlink_from_uri(uri_g);
}

/*
 * Class:     org_gnu_gnomevfs_File
 * Method:    gnome_vfs_move_uri
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_File_gnome_1vfs_1move_1uri
  (JNIEnv *env, jclass cls, jobject oldUri, jobject newUri, jboolean replace)
{
	GnomeVFSURI* oldUri_g = (GnomeVFSURI*)getPointerFromHandle(env, oldUri);
	GnomeVFSURI* newUri_g = (GnomeVFSURI*)getPointerFromHandle(env, newUri);
	return (jint)gnome_vfs_move_uri(oldUri_g, newUri_g, replace);
}

/*
 * Class:     org_gnu_gnomevfs_File
 * Method:    gnome_vfs_move
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_File_gnome_1vfs_1move
  (JNIEnv *env, jclass cls, jstring oldUri, jstring newUri, jboolean replace)
{
	const gchar* oldUri_g = (*env)->GetStringUTFChars(env, oldUri, NULL);
	const gchar* newUri_g = (*env)->GetStringUTFChars(env, newUri, NULL);
	jint retval = (jint)gnome_vfs_move(oldUri_g, newUri_g, replace);
	(*env)->ReleaseStringUTFChars(env, oldUri, oldUri_g);
	(*env)->ReleaseStringUTFChars(env, newUri, newUri_g);
	return retval;
}

/*
 * Class:     org_gnu_gnomevfs_File
 * Method:    gnome_vfs_check_same_fs_uris
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_File_gnome_1vfs_1check_1same_1fs_1uris
  (JNIEnv *env, jclass cls, jobject source, jobject target, jbooleanArray same)
{
	GnomeVFSURI* source_g = (GnomeVFSURI*)getPointerFromHandle(env, source);
	GnomeVFSURI* target_g = (GnomeVFSURI*)getPointerFromHandle(env, target);
	gboolean* same_g = (gboolean*)(*env)->GetBooleanArrayElements(env, same, NULL);
	jint result = (jint)gnome_vfs_check_same_fs_uris(source_g, target_g, same_g);
	(*env)->ReleaseBooleanArrayElements(env, same, (jboolean*)same_g, 0);
	return result;
}

/*
 * Class:     org_gnu_gnomevfs_File
 * Method:    gnome_vfs_check_same_fs
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_File_gnome_1vfs_1check_1same_1fs
  (JNIEnv *env, jclass cls, jstring source, jstring target, jbooleanArray same)
{
	const gchar* source_g = (*env)->GetStringUTFChars(env, source, NULL);
	const gchar* target_g = (*env)->GetStringUTFChars(env, target, NULL);
	gboolean* same_g = (gboolean*)(*env)->GetBooleanArrayElements(env, same, NULL);
	jint result = (jint)gnome_vfs_check_same_fs(source_g, target_g, same_g);
	(*env)->ReleaseStringUTFChars(env, source, source_g);
	(*env)->ReleaseStringUTFChars(env, target, target_g);
	(*env)->ReleaseBooleanArrayElements(env, same, (jboolean*)same_g, 0);
	return result;
}

/*
 * Class:     org_gnu_gnomevfs_File
 * Method:    gnome_vfs_uri_exists
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevfs_File_gnome_1vfs_1uri_1exists
  (JNIEnv *env, jclass cls, jobject uri)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	return (jboolean)gnome_vfs_uri_exists(uri_g);
}

/*
 * Class:     org_gnu_gnomevfs_File
 * Method:    gnome_vfs_create_symbolic_link
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_File_gnome_1vfs_1create_1symbolic_1link
  (JNIEnv *env, jclass cls, jobject uri, jstring target)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	const gchar* target_g = (*env)->GetStringUTFChars(env, target, NULL);
	jint result = (jint)gnome_vfs_create_symbolic_link(uri_g, target_g);
	(*env)->ReleaseStringUTFChars(env, target, target_g);
	return result;
}

/*
 * Class:     org_gnu_gnomevfs_File
 * Method:    gnome_vfs_read
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_File_gnome_1vfs_1read
  (JNIEnv *env, jclass cls, jobject handle, jbyteArray buff, jlong bytes, jlongArray bytesRead)
{
	GnomeVFSHandle* handle_g = (GnomeVFSHandle*)getPointerFromHandle(env, handle);
	gpointer buffer = (gpointer)(*env)->GetByteArrayElements(env, buff, NULL);
	GnomeVFSFileSize* size = (GnomeVFSFileSize*)(*env)->GetLongArrayElements(env, bytesRead, NULL);
	jint result = (jint)gnome_vfs_read(handle_g, buffer, (GnomeVFSFileSize)bytes, size);
	(*env)->ReleaseByteArrayElements(env, buff, (jbyte*)buffer, 0);
	(*env)->ReleaseLongArrayElements(env, bytesRead, (jlong*)size, 0);
	return result;
}

/*
 * Class:     org_gnu_gnomevfs_File
 * Method:    gnome_vfs_write
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_File_gnome_1vfs_1write
  (JNIEnv *env, jclass cls, jobject handle, jbyteArray buff, jlong bytes, jlongArray bytesWritten)
{
	GnomeVFSHandle* handle_g = (GnomeVFSHandle*)getPointerFromHandle(env, handle);
	gconstpointer buffer = (gconstpointer)(*env)->GetByteArrayElements(env, buff, NULL);
	GnomeVFSFileSize* size = (GnomeVFSFileSize*)(*env)->GetLongArrayElements(env, bytesWritten, NULL);
	jint result = (jint)gnome_vfs_write(handle_g, buffer, (GnomeVFSFileSize)bytes, size);
	(*env)->ReleaseByteArrayElements(env, buff, (jbyte*)buffer, 0);
	(*env)->ReleaseLongArrayElements(env, bytesWritten, (jlong*)size, 0);
	return result;
	
}

/*
 * Class:     org_gnu_gnomevfs_File
 * Method:    gnome_vfs_seek
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_File_gnome_1vfs_1seek
  (JNIEnv *env, jclass cls, jobject handle, jint whence, jlong offset)
{
	GnomeVFSHandle* handle_g = (GnomeVFSHandle*)getPointerFromHandle(env, handle);
	return gnome_vfs_seek(handle_g, (GnomeVFSSeekPosition)whence, (GnomeVFSFileOffset)offset);
}

/*
 * Class:     org_gnu_gnomevfs_File
 * Method:    gnome_vfs_tell
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_File_gnome_1vfs_1tell
  (JNIEnv *env, jclass cls, jobject handle, jlongArray offset)
{
	GnomeVFSHandle* handle_g = (GnomeVFSHandle*)getPointerFromHandle(env, handle);
	GnomeVFSFileSize* offset_g = (GnomeVFSFileSize*)(*env)->GetLongArrayElements(env, offset, NULL);
	jint result = (jint)gnome_vfs_tell(handle_g, offset_g);
	(*env)->ReleaseLongArrayElements(env, offset, (jlong*)offset_g, 0);
	return result;
}

#ifdef __cplusplus
}
#endif
#endif
