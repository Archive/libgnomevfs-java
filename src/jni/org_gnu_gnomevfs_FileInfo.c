/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <libgnomevfs/gnome-vfs.h>
#include <jg_jnu.h>

#ifndef _Included_org_gnu_gnomevfs_FileInfo
#define _Included_org_gnu_gnomevfs_FileInfo
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gnomevfs_FileInfo
 * Method:    gnome_vfs_file_info_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnomevfs_FileInfo_gnome_1vfs_1file_1info_1new
  (JNIEnv *env, jclass cls)
{
	return getHandleFromPointer(env, gnome_vfs_file_info_new());
}

/*
 * Class:     org_gnu_gnomevfs_FileInfo
 * Method:    getName
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_FileInfo_getName
  (JNIEnv *env, jclass cls, jobject handle)
{
	GnomeVFSFileInfo* obj = (GnomeVFSFileInfo*)getPointerFromHandle(env, handle);
	return (*env)->NewStringUTF(env, obj->name);
}

/*
 * Class:     org_gnu_gnomevfs_FileInfo
 * Method:    getValidFields
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_FileInfo_getValidFields
  (JNIEnv *env, jclass cls, jobject handle)
{
	GnomeVFSFileInfo* obj = (GnomeVFSFileInfo*)getPointerFromHandle(env, handle);
	return (jint)obj->valid_fields;
}

/*
 * Class:     org_gnu_gnomevfs_FileInfo
 * Method:    getFileType
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_FileInfo_getFileType
  (JNIEnv *env, jclass cls, jobject handle)
{
	GnomeVFSFileInfo* obj = (GnomeVFSFileInfo*)getPointerFromHandle(env, handle);
	return (jint)obj->type;
}

/*
 * Class:     org_gnu_gnomevfs_FileInfo
 * Method:    getFilePermissions
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_FileInfo_getFilePermissions
  (JNIEnv *env, jclass cls, jobject handle)
{
	GnomeVFSFileInfo* obj = (GnomeVFSFileInfo*)getPointerFromHandle(env, handle);
	return (jint)obj->permissions;
}

/*
 * Class:     org_gnu_gnomevfs_FileInfo
 * Method:    getFileFlags
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_FileInfo_getFileFlags
  (JNIEnv *env, jclass cls, jobject handle)
{
	GnomeVFSFileInfo* obj = (GnomeVFSFileInfo*)getPointerFromHandle(env, handle);
	return (jint)obj->flags;
}

/*
 * Class:     org_gnu_gnomevfs_FileInfo
 * Method:    getFileSize
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_FileInfo_getFileSize
  (JNIEnv *env, jclass cls, jobject handle)
{
	GnomeVFSFileInfo* obj = (GnomeVFSFileInfo*)getPointerFromHandle(env, handle);
	return (jint)obj->size;
}

/*
 * Class:     org_gnu_gnomevfs_FileInfo
 * Method:    getBlockCount
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_FileInfo_getBlockCount
  (JNIEnv *env, jclass cls, jobject handle)
{
	GnomeVFSFileInfo* obj = (GnomeVFSFileInfo*)getPointerFromHandle(env, handle);
	return (jint)obj->block_count;
}

/*
 * Class:     org_gnu_gnomevfs_FileInfo
 * Method:    getMimeType
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_FileInfo_getMimeType
  (JNIEnv *env, jclass cls, jobject handle)
{
	GnomeVFSFileInfo* obj = (GnomeVFSFileInfo*)getPointerFromHandle(env, handle);
	return (*env)->NewStringUTF(env, obj->mime_type);
}

/*
 * Class:     org_gnu_gnomevfs_FileInfo
 * Method:    getSymlinkName
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_FileInfo_getSymlinkName
  (JNIEnv *env, jclass cls, jobject handle)
{
	GnomeVFSFileInfo* obj = (GnomeVFSFileInfo*)getPointerFromHandle(env, handle);
	return (*env)->NewStringUTF(env, obj->symlink_name);
}

/*
 * Class:     org_gnu_gnomevfs_FileInfo
 * Method:    getAtime
 */
JNIEXPORT jlong JNICALL Java_org_gnu_gnomevfs_FileInfo_getAtime
  (JNIEnv *env, jclass cls, jobject handle)
{
	GnomeVFSFileInfo* obj = (GnomeVFSFileInfo*)getPointerFromHandle(env, handle);
	return (jlong)obj->atime;
}

/*
 * Class:     org_gnu_gnomevfs_FileInfo
 * Method:    getCtime
 */
JNIEXPORT jlong JNICALL Java_org_gnu_gnomevfs_FileInfo_getCtime
  (JNIEnv *env, jclass cls, jobject handle)
{
	GnomeVFSFileInfo* obj = (GnomeVFSFileInfo*)getPointerFromHandle(env, handle);
	return (jlong)obj->ctime;
}

/*
 * Class:     org_gnu_gnomevfs_FileInfo
 * Method:    getMtime
 */
JNIEXPORT jlong JNICALL Java_org_gnu_gnomevfs_FileInfo_getMtime
  (JNIEnv *env, jclass cls, jobject handle)
{
	GnomeVFSFileInfo* obj = (GnomeVFSFileInfo*)getPointerFromHandle(env, handle);
	return (jlong)obj->mtime;
}

#ifdef __cplusplus
}
#endif
#endif
