/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <libgnomevfs/gnome-vfs.h>
#include <jg_jnu.h>
#include <sys/stat.h>

#ifndef _Included_org_gnu_gnomevfs_FilePermissions
#define _Included_org_gnu_gnomevfs_FilePermissions
#ifdef __cplusplus
extern "C" {
#endif


/*
 * Class:     org_gnu_gnomevfs_FilePermissions
 * Method:    getSUID
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_FilePermissions_getSUID
  (JNIEnv *env, jclass cls) 
{
	return S_ISUID;
}

/*
 * Class:     org_gnu_gnomevfs_FilePermissions
 * Method:    getSGID
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_FilePermissions_getSGID
  (JNIEnv *env, jclass cls) 
{
	return S_ISGID;
}

/*
 * Class:     org_gnu_gnomevfs_FilePermissions
 * Method:    getRUSR
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_FilePermissions_getRUSR
  (JNIEnv *env, jclass cls) 
{
	return S_IRUSR;
}

/*
 * Class:     org_gnu_gnomevfs_FilePermissions
 * Method:    getWUSR
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_FilePermissions_getWUSR
  (JNIEnv *env, jclass cls) 
{
	return S_IWUSR;
}

/*
 * Class:     org_gnu_gnomevfs_FilePermissions
 * Method:    getXUSR
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_FilePermissions_getXUSR
  (JNIEnv *env, jclass cls) 
{
	return S_IXUSR;
}

/*
 * Class:     org_gnu_gnomevfs_FilePermissions
 * Method:    getRGRP
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_FilePermissions_getRGRP
  (JNIEnv *env, jclass cls) 
{
	return S_IRGRP;
}

/*
 * Class:     org_gnu_gnomevfs_FilePermissions
 * Method:    getWGRP
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_FilePermissions_getWGRP
  (JNIEnv *env, jclass cls) 
{
	return S_IWGRP;
}

/*
 * Class:     org_gnu_gnomevfs_FilePermissions
 * Method:    getXGRP
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_FilePermissions_getXGRP
  (JNIEnv *env, jclass cls) 
{
	return S_IXGRP;
}

/*
 * Class:     org_gnu_gnomevfs_FilePermissions
 * Method:    getROTH
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_FilePermissions_getROTH
  (JNIEnv *env, jclass cls) 
{
	return S_IROTH;
}

/*
 * Class:     org_gnu_gnomevfs_FilePermissions
 * Method:    getWOTH
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_FilePermissions_getWOTH
  (JNIEnv *env, jclass cls) 
{
	return S_IWOTH;
}

/*
 * Class:     org_gnu_gnomevfs_FilePermissions
 * Method:    getXOTH
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_FilePermissions_getXOTH
  (JNIEnv *env, jclass cls) 
{
	return S_IXOTH;
}

#ifdef __cplusplus
}
#endif
#endif
