/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */


#include <jni.h>
#include <libgnomevfs/gnome-vfs.h>
#include <jg_jnu.h>

#ifndef _Included_org_gnu_gnomevfs_GnomeVfs
#define _Included_org_gnu_gnomevfs_GnomeVfs
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gnomevfs_GnomeVfs
 * Method:    gnome_vfs_init
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevfs_GnomeVfs_gnome_1vfs_1init
  (JNIEnv *env, jclass cls)
{
	return gnome_vfs_init();
}

/*
 * Class:     org_gnu_gnomevfs_GnomeVfs
 * Method:    gnome_vfs_initialized
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevfs_GnomeVfs_gnome_1vfs_1initialized
  (JNIEnv *env, jclass cls)
{
	return gnome_vfs_initialized();
}

/*
 * Class:     org_gnu_gnomevfs_GnomeVfs
 * Method:    gnome_vfs_shutdown
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevfs_GnomeVfs_gnome_1vfs_1shutdown
  (JNIEnv *env , jclass cls)
{
	gnome_vfs_shutdown();
}

#ifdef __cplusplus
}
#endif
#endif
