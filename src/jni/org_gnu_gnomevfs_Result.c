/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */


#include <jni.h>
#include <libgnomevfs/gnome-vfs.h>

#ifndef _Included_org_gnu_gnomevfs_Result
#define _Included_org_gnu_gnomevfs_Result
#ifdef __cplusplus
extern "C" {
#endif
#undef org_gnu_gnomevfs_Result__OK
#define org_gnu_gnomevfs_Result__OK 0L
/* Inaccessible static: OK */
#undef org_gnu_gnomevfs_Result__ERROR_NOT_FOUND
#define org_gnu_gnomevfs_Result__ERROR_NOT_FOUND 1L
/* Inaccessible static: ERROR_NOT_FOUND */
#undef org_gnu_gnomevfs_Result__ERROR_GENERIC
#define org_gnu_gnomevfs_Result__ERROR_GENERIC 2L
/* Inaccessible static: ERROR_GENERIC */
#undef org_gnu_gnomevfs_Result__ERROR_INTERNAL
#define org_gnu_gnomevfs_Result__ERROR_INTERNAL 3L
/* Inaccessible static: ERROR_INTERNAL */
#undef org_gnu_gnomevfs_Result__ERROR_BAD_PARAMETERS
#define org_gnu_gnomevfs_Result__ERROR_BAD_PARAMETERS 4L
/* Inaccessible static: ERROR_BAD_PARAMETERS */
#undef org_gnu_gnomevfs_Result__ERROR_NOT_SUPPORTED
#define org_gnu_gnomevfs_Result__ERROR_NOT_SUPPORTED 5L
/* Inaccessible static: ERROR_NOT_SUPPORTED */
#undef org_gnu_gnomevfs_Result__ERROR_IO
#define org_gnu_gnomevfs_Result__ERROR_IO 6L
/* Inaccessible static: ERROR_IO */
#undef org_gnu_gnomevfs_Result__ERROR_CORRUPTED_DATA
#define org_gnu_gnomevfs_Result__ERROR_CORRUPTED_DATA 7L
/* Inaccessible static: ERROR_CORRUPTED_DATA */
#undef org_gnu_gnomevfs_Result__ERROR_WRONG_FORMAT
#define org_gnu_gnomevfs_Result__ERROR_WRONG_FORMAT 8L
/* Inaccessible static: ERROR_WRONG_FORMAT */
#undef org_gnu_gnomevfs_Result__ERROR_BAD_FILE
#define org_gnu_gnomevfs_Result__ERROR_BAD_FILE 9L
/* Inaccessible static: ERROR_BAD_FILE */
#undef org_gnu_gnomevfs_Result__ERROR_TOO_BIG
#define org_gnu_gnomevfs_Result__ERROR_TOO_BIG 10L
/* Inaccessible static: ERROR_TOO_BIG */
#undef org_gnu_gnomevfs_Result__ERROR_NO_SPACE
#define org_gnu_gnomevfs_Result__ERROR_NO_SPACE 11L
/* Inaccessible static: ERROR_NO_SPACE */
#undef org_gnu_gnomevfs_Result__ERROR_READ_ONLY
#define org_gnu_gnomevfs_Result__ERROR_READ_ONLY 12L
/* Inaccessible static: ERROR_READ_ONLY */
#undef org_gnu_gnomevfs_Result__ERROR_INVALID_URI
#define org_gnu_gnomevfs_Result__ERROR_INVALID_URI 13L
/* Inaccessible static: ERROR_INVALID_URI */
#undef org_gnu_gnomevfs_Result__ERROR_NOT_OPEN
#define org_gnu_gnomevfs_Result__ERROR_NOT_OPEN 14L
/* Inaccessible static: ERROR_NOT_OPEN */
#undef org_gnu_gnomevfs_Result__ERROR_INVALID_OPEN_MODE
#define org_gnu_gnomevfs_Result__ERROR_INVALID_OPEN_MODE 15L
/* Inaccessible static: ERROR_INVALID_OPEN_MODE */
#undef org_gnu_gnomevfs_Result__ERROR_ACCESS_DENIED
#define org_gnu_gnomevfs_Result__ERROR_ACCESS_DENIED 16L
/* Inaccessible static: ERROR_ACCESS_DENIED */
#undef org_gnu_gnomevfs_Result__ERROR_TOO_MANY_OPEN_FILES
#define org_gnu_gnomevfs_Result__ERROR_TOO_MANY_OPEN_FILES 17L
/* Inaccessible static: ERROR_TOO_MANY_OPEN_FILES */
#undef org_gnu_gnomevfs_Result__ERROR_EOF
#define org_gnu_gnomevfs_Result__ERROR_EOF 18L
/* Inaccessible static: ERROR_EOF */
#undef org_gnu_gnomevfs_Result__ERROR_NOT_A_DIRECTORY
#define org_gnu_gnomevfs_Result__ERROR_NOT_A_DIRECTORY 19L
/* Inaccessible static: ERROR_NOT_A_DIRECTORY */
#undef org_gnu_gnomevfs_Result__ERROR_IN_PROGRESS
#define org_gnu_gnomevfs_Result__ERROR_IN_PROGRESS 20L
/* Inaccessible static: ERROR_IN_PROGRESS */
#undef org_gnu_gnomevfs_Result__ERROR_INTERRUPTED
#define org_gnu_gnomevfs_Result__ERROR_INTERRUPTED 21L
/* Inaccessible static: ERROR_INTERRUPTED */
#undef org_gnu_gnomevfs_Result__ERROR_FILE_EXISTS
#define org_gnu_gnomevfs_Result__ERROR_FILE_EXISTS 22L
/* Inaccessible static: ERROR_FILE_EXISTS */
#undef org_gnu_gnomevfs_Result__ERROR_LOOP
#define org_gnu_gnomevfs_Result__ERROR_LOOP 23L
/* Inaccessible static: ERROR_LOOP */
#undef org_gnu_gnomevfs_Result__ERROR_NOT_PERMITTED
#define org_gnu_gnomevfs_Result__ERROR_NOT_PERMITTED 24L
/* Inaccessible static: ERROR_NOT_PERMITTED */
#undef org_gnu_gnomevfs_Result__ERROR_IS_DIRECTORY
#define org_gnu_gnomevfs_Result__ERROR_IS_DIRECTORY 25L
/* Inaccessible static: ERROR_IS_DIRECTORY */
#undef org_gnu_gnomevfs_Result__ERROR_NO_MEMORY
#define org_gnu_gnomevfs_Result__ERROR_NO_MEMORY 26L
/* Inaccessible static: ERROR_NO_MEMORY */
#undef org_gnu_gnomevfs_Result__ERROR_HOST_NOT_FOUND
#define org_gnu_gnomevfs_Result__ERROR_HOST_NOT_FOUND 27L
/* Inaccessible static: ERROR_HOST_NOT_FOUND */
#undef org_gnu_gnomevfs_Result__ERROR_INVALID_HOST_NAME
#define org_gnu_gnomevfs_Result__ERROR_INVALID_HOST_NAME 28L
/* Inaccessible static: ERROR_INVALID_HOST_NAME */
#undef org_gnu_gnomevfs_Result__ERROR_HOST_HAS_NO_ADDRESS
#define org_gnu_gnomevfs_Result__ERROR_HOST_HAS_NO_ADDRESS 29L
/* Inaccessible static: ERROR_HOST_HAS_NO_ADDRESS */
#undef org_gnu_gnomevfs_Result__ERROR_LOGIN_FAILED
#define org_gnu_gnomevfs_Result__ERROR_LOGIN_FAILED 30L
/* Inaccessible static: ERROR_LOGIN_FAILED */
#undef org_gnu_gnomevfs_Result__ERROR_CANCELLED
#define org_gnu_gnomevfs_Result__ERROR_CANCELLED 31L
/* Inaccessible static: ERROR_CANCELLED */
#undef org_gnu_gnomevfs_Result__ERROR_DIRECTORY_BUSY
#define org_gnu_gnomevfs_Result__ERROR_DIRECTORY_BUSY 32L
/* Inaccessible static: ERROR_DIRECTORY_BUSY */
#undef org_gnu_gnomevfs_Result__ERROR_DIRECTORY_NOT_EMPTY
#define org_gnu_gnomevfs_Result__ERROR_DIRECTORY_NOT_EMPTY 33L
/* Inaccessible static: ERROR_DIRECTORY_NOT_EMPTY */
#undef org_gnu_gnomevfs_Result__ERROR_TOO_MANY_LINKS
#define org_gnu_gnomevfs_Result__ERROR_TOO_MANY_LINKS 34L
/* Inaccessible static: ERROR_TOO_MANY_LINKS */
#undef org_gnu_gnomevfs_Result__ERROR_READ_ONLY_FILE_SYSTEM
#define org_gnu_gnomevfs_Result__ERROR_READ_ONLY_FILE_SYSTEM 35L
/* Inaccessible static: ERROR_READ_ONLY_FILE_SYSTEM */
#undef org_gnu_gnomevfs_Result__ERROR_NOT_SAME_FILE_SYSTEM
#define org_gnu_gnomevfs_Result__ERROR_NOT_SAME_FILE_SYSTEM 36L
/* Inaccessible static: ERROR_NOT_SAME_FILE_SYSTEM */
#undef org_gnu_gnomevfs_Result__ERROR_NAME_TOO_LONG
#define org_gnu_gnomevfs_Result__ERROR_NAME_TOO_LONG 37L
/* Inaccessible static: ERROR_NAME_TOO_LONG */
#undef org_gnu_gnomevfs_Result__ERROR_SERVICE_NOT_AVAILABLE
#define org_gnu_gnomevfs_Result__ERROR_SERVICE_NOT_AVAILABLE 38L
/* Inaccessible static: ERROR_SERVICE_NOT_AVAILABLE */
#undef org_gnu_gnomevfs_Result__ERROR_SERVICE_OBSOLETE
#define org_gnu_gnomevfs_Result__ERROR_SERVICE_OBSOLETE 39L
/* Inaccessible static: ERROR_SERVICE_OBSOLETE */
#undef org_gnu_gnomevfs_Result__ERROR_PROTOCOL_ERROR
#define org_gnu_gnomevfs_Result__ERROR_PROTOCOL_ERROR 40L
/* Inaccessible static: ERROR_PROTOCOL_ERROR */
#undef org_gnu_gnomevfs_Result__ERROR_NO_MASTER_BROWSER
#define org_gnu_gnomevfs_Result__ERROR_NO_MASTER_BROWSER 41L
/* Inaccessible static: ERROR_NO_MASTER_BROWSER */
#undef org_gnu_gnomevfs_Result__ERROR_NO_DEFAULT
#define org_gnu_gnomevfs_Result__ERROR_NO_DEFAULT 42L
/* Inaccessible static: ERROR_NO_DEFAULT */
#undef org_gnu_gnomevfs_Result__ERROR_NO_HANDLER
#define org_gnu_gnomevfs_Result__ERROR_NO_HANDLER 43L
/* Inaccessible static: ERROR_NO_HANDLER */
#undef org_gnu_gnomevfs_Result__ERROR_PARSE
#define org_gnu_gnomevfs_Result__ERROR_PARSE 44L
/* Inaccessible static: ERROR_PARSE */
#undef org_gnu_gnomevfs_Result__ERROR_LAUNCH
#define org_gnu_gnomevfs_Result__ERROR_LAUNCH 45L
/* Inaccessible static: ERROR_LAUNCH */
#undef org_gnu_gnomevfs_Result__ERROR_TIMEOUT
#define org_gnu_gnomevfs_Result__ERROR_TIMEOUT 46L
/* Inaccessible static: ERROR_TIMEOUT */
/* Inaccessible static: theInterned */
/* Inaccessible static: theInternedExtras */
/* Inaccessible static: theSacrificialOne */

/*
 * Class:     org_gnu_gnomevfs_Result
 * Method:    gnome_vfs_result_to_string
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Result_gnome_1vfs_1result_1to_1string
  (JNIEnv *env, jclass cls, jint result)
{
	const char* message = gnome_vfs_result_to_string((GnomeVFSResult)result);
	return (*env)->NewStringUTF(env, message);
}

/*
 * Class:     org_gnu_gnomevfs_Result
 * Method:    gnome_vfs_result_from_errno_code
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_Result_gnome_1vfs_1result_1from_1errno_1code
  (JNIEnv *env, jclass cls, jint errno)
{
	return (jint)gnome_vfs_result_from_errno_code((int)errno);
}

#ifdef __cplusplus
}
#endif
#endif
