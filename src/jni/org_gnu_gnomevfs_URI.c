/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <libgnomevfs/gnome-vfs.h>
#include <jg_jnu.h>

#ifndef _Included_org_gnu_gnomevfsURI
#define _Included_org_gnu_gnomevfsURI
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1new
  (JNIEnv *env, jclass cls, jstring uri)
{
	const gchar* u = (*env)->GetStringUTFChars(env, uri, NULL);
	jobject result = getHandleFromPointer(env, gnome_vfs_uri_new(u));
	(*env)->ReleaseStringUTFChars(env, uri, u);
	return result;
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_resolve_relative
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1resolve_1relative
  (JNIEnv *env, jclass cls, jobject base, jstring relative)
{
	const gchar* r = (*env)->GetStringUTFChars(env, relative, NULL);
	GnomeVFSURI* base_g = (GnomeVFSURI*)getPointerFromHandle(env, base);
	jobject result = getHandleFromPointer(env, gnome_vfs_uri_resolve_relative(base_g, r));
	(*env)->ReleaseStringUTFChars(env, relative, r);
	return result;
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_append_string
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1append_1string
  (JNIEnv *env, jclass cls, jobject uri, jstring fragment)
{
	const gchar* str = (*env)->GetStringUTFChars(env, fragment, NULL);
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	jobject result = getHandleFromPointer(env, gnome_vfs_uri_append_string(uri_g, str));
	(*env)->ReleaseStringUTFChars(env, fragment, str);
	return result;
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_append_path
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1append_1path
  (JNIEnv *env, jclass cls, jobject uri, jstring path)
{
	const gchar* str = (*env)->GetStringUTFChars(env, path, NULL);
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	jobject result = getHandleFromPointer(env, gnome_vfs_uri_append_path(uri_g, str));
	(*env)->ReleaseStringUTFChars(env, path, str);
	return result;
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_append_file_name
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1append_1file_1name
  (JNIEnv *env, jclass cls, jobject uri, jstring filename)
{
	const gchar* str = (*env)->GetStringUTFChars(env, filename, NULL);
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	jobject result = getHandleFromPointer(env, gnome_vfs_uri_append_file_name(uri_g, str));
	(*env)->ReleaseStringUTFChars(env, filename, str);
	return result;
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_to_string
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1to_1string
  (JNIEnv *env, jclass cls, jobject uri, jint hideOptions)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	gchar* str = gnome_vfs_uri_to_string(uri_g, (GnomeVFSURIHideOptions)hideOptions);
	return (*env)->NewStringUTF(env, str);
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_dup
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1dup
  (JNIEnv *env, jclass cls, jobject uri)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	return getHandleFromPointer(env, gnome_vfs_uri_dup(uri_g));
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_is_local
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1is_1local
  (JNIEnv *env, jclass cls, jobject uri)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	return (jboolean)gnome_vfs_uri_is_local(uri_g);
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_has_parent
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1has_1parent
  (JNIEnv *env, jclass cls, jobject uri)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	return (jboolean)gnome_vfs_uri_has_parent(uri_g);
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_get_parent
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1get_1parent
  (JNIEnv *env, jclass cls, jobject uri)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	return getHandleFromPointer(env, gnome_vfs_uri_get_parent(uri_g));
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_get_toplevel
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1get_1toplevel
  (JNIEnv *env, jclass cls, jobject uri)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	return getHandleFromPointer(env, gnome_vfs_uri_get_toplevel(uri_g));
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_get_host_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1get_1host_1name
  (JNIEnv *env, jclass cls, jobject uri)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	return (*env)->NewStringUTF(env, gnome_vfs_uri_get_host_name(uri_g));
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_get_scheme
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1get_1scheme
  (JNIEnv *env, jclass cls, jobject uri)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	return (*env)->NewStringUTF(env, gnome_vfs_uri_get_scheme(uri_g));
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_get_host_port
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1get_1host_1port
  (JNIEnv *env, jclass cls, jobject uri)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	return (jint)gnome_vfs_uri_get_host_port(uri_g);
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_get_user_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1get_1user_1name
  (JNIEnv *env, jclass cls, jobject uri)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	return (*env)->NewStringUTF(env, gnome_vfs_uri_get_user_name(uri_g));
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_get_password
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1get_1password
  (JNIEnv *env, jclass cls, jobject uri)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	return (*env)->NewStringUTF(env, gnome_vfs_uri_get_password(uri_g));
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_set_host_name
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1set_1host_1name
  (JNIEnv *env, jclass cls, jobject uri, jstring host)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	const gchar* str = (*env)->GetStringUTFChars(env, host, NULL);
	gnome_vfs_uri_set_host_name(uri_g, str);
	(*env)->ReleaseStringUTFChars(env, host, str);
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_set_host_port
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1set_1host_1port
  (JNIEnv *env, jclass cls, jobject uri, jint port)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	gnome_vfs_uri_set_host_port(uri_g, port);
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_set_user_name
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1set_1user_1name
  (JNIEnv *env, jclass cls, jobject uri, jstring name)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	const gchar* str = (*env)->GetStringUTFChars(env, name, NULL);
	gnome_vfs_uri_set_user_name(uri_g, str);
	(*env)->ReleaseStringUTFChars(env, name, str);
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_set_password
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1set_1password
  (JNIEnv *env, jclass cls, jobject uri, jstring pass)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	const gchar* str = (*env)->GetStringUTFChars(env, pass, NULL);
	gnome_vfs_uri_set_password(uri_g, str);
	(*env)->ReleaseStringUTFChars(env, pass, str);
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_equal
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1equal
  (JNIEnv *env, jclass cls, jobject a, jobject b)
{
	GnomeVFSURI* a_g = (GnomeVFSURI*)getPointerFromHandle(env, a);
	GnomeVFSURI* b_g = (GnomeVFSURI*)getPointerFromHandle(env, b);
	return (jboolean)gnome_vfs_uri_equal(a_g, b_g);
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_is_parent
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1is_1parent
  (JNIEnv *env, jclass cls, jobject parent, jobject child, jboolean recursive)
{
	GnomeVFSURI* parent_g = (GnomeVFSURI*)getPointerFromHandle(env, parent);
	GnomeVFSURI* child_g = (GnomeVFSURI*)getPointerFromHandle(env, child);
	return (jboolean)gnome_vfs_uri_is_parent(parent_g, child_g, recursive);
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_get_path
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1get_1path
  (JNIEnv *env, jclass cls, jobject uri)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	return (*env)->NewStringUTF(env, gnome_vfs_uri_get_path(uri_g));
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_get_fragment_identifier
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1get_1fragment_1identifier
  (JNIEnv *env, jclass cls, jobject uri)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	return (*env)->NewStringUTF(env, gnome_vfs_uri_get_fragment_identifier(uri_g));
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_extract_dirname
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1extract_1dirname
  (JNIEnv *env, jclass cls, jobject uri)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	return (*env)->NewStringUTF(env, gnome_vfs_uri_extract_dirname(uri_g));
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_extract_short_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1extract_1short_1name
  (JNIEnv *env, jclass cls, jobject uri)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	return (*env)->NewStringUTF(env, gnome_vfs_uri_extract_short_name(uri_g));
}

/*
 * Class:     org_gnu_gnomevfsURI
 * Method:    gnome_vfs_uri_extract_short_path_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfsURI_gnome_1vfs_1uri_1extract_1short_1path_1name
  (JNIEnv *env, jclass cls, jobject uri)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	return (*env)->NewStringUTF(env, gnome_vfs_uri_extract_short_path_name(uri_g));
}

#ifdef __cplusplus
}
#endif
#endif
