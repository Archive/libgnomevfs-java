/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <libgnomevfs/gnome-vfs.h>
#include <jg_jnu.h>

#ifndef _Included_org_gnu_gnomevfs_Utils
#define _Included_org_gnu_gnomevfs_Utils
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_format_file_size_for_display
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1format_1file_1size_1for_1display
  (JNIEnv *env, jclass cls, jlong size)
{
	return (*env)->NewStringUTF(env, gnome_vfs_format_file_size_for_display((GnomeVFSFileSize)size));
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_format_uri_for_display
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1format_1uri_1for_1display
  (JNIEnv *env, jclass cls, jstring uri)
{
	const char* u = (*env)->GetStringUTFChars(env, uri, NULL);
	char* result = gnome_vfs_format_uri_for_display(u);
	(*env)->ReleaseStringUTFChars(env, uri, u);
	return (*env)->NewStringUTF(env, result);
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_url_show
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1url_1show
  (JNIEnv *env, jclass cls, jstring url)
{
	const char* u = (*env)->GetStringUTFChars(env, url, NULL);
	jint result = (jint)gnome_vfs_url_show(u);
	(*env)->ReleaseStringUTFChars(env, url, u);
	return result;
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_url_show_with_env
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1url_1show_1with_1env
  (JNIEnv *env, jclass cls, jstring url, jobjectArray en)
{
	const char* u = (*env)->GetStringUTFChars(env, url, NULL);
	char** e = getStringArray(env, en);
	jint result = (jint)gnome_vfs_url_show_with_env(u, e);
	(*env)->ReleaseStringUTFChars(env, url, u);
	freeStringArray(env, en, e);
	return result;	
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_escape_string
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1escape_1string
  (JNIEnv *env, jclass cls, jstring str)
{
	char* s = (char*)(*env)->GetStringUTFChars(env, str, NULL);
	char* result = gnome_vfs_escape_string(s);
	(*env)->ReleaseStringUTFChars(env, str, s);
	return (*env)->NewStringUTF(env, result);
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_escape_path_string
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1escape_1path_1string
  (JNIEnv *env, jclass cls, jstring str)
{
	char* s = (char*)(*env)->GetStringUTFChars(env, str, NULL);
	char* result = gnome_vfs_escape_path_string(s);
	(*env)->ReleaseStringUTFChars(env, str, s);
	return (*env)->NewStringUTF(env, result);
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_escape_host_and_path_string
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1escape_1host_1and_1path_1string
  (JNIEnv *env, jclass cls, jstring str)
{
	char* s = (char*)(*env)->GetStringUTFChars(env, str, NULL);
	char* result = gnome_vfs_escape_host_and_path_string(s);
	(*env)->ReleaseStringUTFChars(env, str, s);
	return (*env)->NewStringUTF(env, result);
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_escape_slashes
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1escape_1slashes
  (JNIEnv *env, jclass cls, jstring str)
{
	char* s = (char*)(*env)->GetStringUTFChars(env, str, NULL);
	char* result = gnome_vfs_escape_slashes(s);
	(*env)->ReleaseStringUTFChars(env, str, s);
	return (*env)->NewStringUTF(env, result);
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_escape_set
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1escape_1set
  (JNIEnv *env, jclass cls, jstring str, jstring match)
{
	char* s = (char*)(*env)->GetStringUTFChars(env, str, NULL);
	char* m = (char*)(*env)->GetStringUTFChars(env, match, NULL);
	char* result = gnome_vfs_escape_set(s, m);
	(*env)->ReleaseStringUTFChars(env, str, s);
	(*env)->ReleaseStringUTFChars(env, match, m);
	return (*env)->NewStringUTF(env, result);
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_unescape_string
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1unescape_1string
  (JNIEnv *env, jclass cls, jstring str, jstring illegal)
{
	char* s = (char*)(*env)->GetStringUTFChars(env, str, NULL);
	char* i = (char*)(*env)->GetStringUTFChars(env, illegal, NULL);
	char* result = gnome_vfs_unescape_string(s, i);
	(*env)->ReleaseStringUTFChars(env, str, s);
	(*env)->ReleaseStringUTFChars(env, illegal, i);
	return (*env)->NewStringUTF(env, result);
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_make_uri_canonical
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1make_1uri_1canonical
  (JNIEnv *env, jclass cls, jstring uri)
{
	char* u = (char*)(*env)->GetStringUTFChars(env, uri, NULL);
	char* result = gnome_vfs_make_uri_canonical(u);
	(*env)->ReleaseStringUTFChars(env, uri, u);
	return (*env)->NewStringUTF(env, result);
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_make_uri_canonical_strip_fragment
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1make_1uri_1canonical_1strip_1fragment
  (JNIEnv *env, jclass cls, jstring uri)
{
	char* u = (char*)(*env)->GetStringUTFChars(env, uri, NULL);
	char* result = gnome_vfs_make_uri_canonical_strip_fragment(u);
	(*env)->ReleaseStringUTFChars(env, uri, u);
	return (*env)->NewStringUTF(env, result);
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_make_uri_from_input
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1make_1uri_1from_1input
  (JNIEnv *env, jclass cls, jstring uri)
{
	char* u = (char*)(*env)->GetStringUTFChars(env, uri, NULL);
	char* result = gnome_vfs_make_uri_from_input(u);
	(*env)->ReleaseStringUTFChars(env, uri, u);
	return (*env)->NewStringUTF(env, result);
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_make_uri_from_shell_arg
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1make_1uri_1from_1shell_1arg
  (JNIEnv *env, jclass cls, jstring uri)
{
	char* u = (char*)(*env)->GetStringUTFChars(env, uri, NULL);
	char* result = gnome_vfs_make_uri_from_shell_arg(u);
	(*env)->ReleaseStringUTFChars(env, uri, u);
	return (*env)->NewStringUTF(env, result);
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_expand_initial_tilde
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1expand_1initial_1tilde
  (JNIEnv *env, jclass cls, jstring path)
{
	char* p = (char*)(*env)->GetStringUTFChars(env, path, NULL);
	char* result = gnome_vfs_expand_initial_tilde(p);
	(*env)->ReleaseStringUTFChars(env, path, p);
	return (*env)->NewStringUTF(env, result);
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_unescape_string_for_display
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1unescape_1string_1for_1display
  (JNIEnv *env, jclass cls, jstring str)
{
	char* s = (char*)(*env)->GetStringUTFChars(env, str, NULL);
	char* result = gnome_vfs_unescape_string_for_display(s);
	(*env)->ReleaseStringUTFChars(env, str, s);
	return (*env)->NewStringUTF(env, result);
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_get_local_path_from_uri
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1get_1local_1path_1from_1uri
  (JNIEnv *env, jclass cls, jstring uri)
{
	char* u = (char*)(*env)->GetStringUTFChars(env, uri, NULL);
	char* result = gnome_vfs_get_local_path_from_uri(u);
	(*env)->ReleaseStringUTFChars(env, uri, u);
	return (*env)->NewStringUTF(env, result);
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_get_uri_from_local_path
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1get_1uri_1from_1local_1path
  (JNIEnv *env, jclass cls, jstring path)
{
	char* p = (char*)(*env)->GetStringUTFChars(env, path, NULL);
	char* result = gnome_vfs_get_uri_from_local_path(p);
	(*env)->ReleaseStringUTFChars(env, path, p);
	return (*env)->NewStringUTF(env, result);
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_is_executable_command_string
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1is_1executable_1command_1string
  (JNIEnv *env, jclass cls, jstring str)
{
	char* s = (char*)(*env)->GetStringUTFChars(env, str, NULL);
	gboolean result = gnome_vfs_is_executable_command_string(s);
	(*env)->ReleaseStringUTFChars(env, str, s);
	return result;
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_get_volume_free_space
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1get_1volume_1free_1space
  (JNIEnv *env, jclass cls, jobject uri, jlongArray size)
{
	GnomeVFSURI* uri_g = (GnomeVFSURI*)getPointerFromHandle(env, uri);
	GnomeVFSFileSize* s = (GnomeVFSFileSize*)(*env)->GetLongArrayElements(env, size, NULL);
	jint result = (jint)gnome_vfs_get_volume_free_space(uri_g, s);
	(*env)->ReleaseLongArrayElements(env, size, (jlong*)s, 0);
	return result;
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_icon_path_from_filename
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1icon_1path_1from_1filename
  (JNIEnv *env, jclass cls, jstring filename)
{
	char* f = (char*)(*env)->GetStringUTFChars(env, filename, NULL);
	char* result = gnome_vfs_icon_path_from_filename(f);
	(*env)->ReleaseStringUTFChars(env, filename, f);
	return (*env)->NewStringUTF(env, result);
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_get_uri_scheme
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1get_1uri_1scheme
  (JNIEnv *env, jclass cls, jstring uri)
{
	char* u = (char*)(*env)->GetStringUTFChars(env, uri, NULL);
	char* result = gnome_vfs_get_uri_scheme(u);
	(*env)->ReleaseStringUTFChars(env, uri, u);
	return (*env)->NewStringUTF(env, result);
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_uris_match
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1uris_1match
  (JNIEnv *env, jclass cls, jstring uri1, jstring uri2)
{
	char* u1 = (char*)(*env)->GetStringUTFChars(env, uri1, NULL);
	char* u2 = (char*)(*env)->GetStringUTFChars(env, uri2, NULL);
	gboolean result = gnome_vfs_uris_match(u1, u2);
	(*env)->ReleaseStringUTFChars(env, uri1, u1);
	(*env)->ReleaseStringUTFChars(env, uri2, u2);
	return result;
}

/*
 * Class:     org_gnu_gnomevfs_Utils
 * Method:    gnome_vfs_read_entire_file
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_Utils_gnome_1vfs_1read_1entire_1file
  (JNIEnv *env, jclass cls, jstring uri, jlongArray fileSize, jstring contents)
{
	char* u = (char*)(*env)->GetStringUTFChars(env, uri, NULL);
	long* size = (long*)(*env)->GetLongArrayElements(env, fileSize, NULL);
	char* c = (char*)(*env)->GetStringUTFChars(env, contents, NULL);
	jint result = (jint)gnome_vfs_read_entire_file(u, (int*)size, &c);
	(*env)->ReleaseStringUTFChars(env, uri, u);
	(*env)->ReleaseLongArrayElements(env, fileSize, (jlong*)size, 0);
	(*env)->ReleaseStringUTFChars(env, contents, u);
}

#ifdef __cplusplus
}
#endif
#endif
