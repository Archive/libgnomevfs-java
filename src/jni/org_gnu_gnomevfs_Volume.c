/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <libgnomevfs/gnome-vfs.h>
#include <jg_jnu.h>

#ifndef _Included_org_gnu_gnomevfs_Volume
#define _Included_org_gnu_gnomevfs_Volume
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gnomevfs_Volume
 * Method:    gnome_vfs_volume_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_Volume_gnome_1vfs_1volume_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gnome_vfs_volume_get_type();
}

/*
 * Class:     org_gnu_gnomevfs_Volume
 * Method:    gnome_vfs_volume_ref
 * Signature: (Lorg/gnu/glib/Handle;)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnomevfs_Volume_gnome_1vfs_1volume_1ref
  (JNIEnv *env, jclass cls, jobject vol)
{
	GnomeVFSVolume *vol_g = (GnomeVFSVolume*)getPointerFromHandle(env, vol);
	return getHandleFromPointer(env, gnome_vfs_volume_ref(vol_g));
}

/*
 * Class:     org_gnu_gnomevfs_Volume
 * Method:    gnome_vfs_volume_unref
 * Signature: (Lorg/gnu/glib/Handle;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gnomevfs_Volume_gnome_1vfs_1volume_1unref
  (JNIEnv *env, jclass cls, jobject vol)
{
	GnomeVFSVolume *vol_g = (GnomeVFSVolume*)getPointerFromHandle(env, vol);
	gnome_vfs_volume_unref(vol_g);
}

/*
 * Class:     org_gnu_gnomevfs_Volume
 * Method:    gnome_vfs_volume_get_id
 * Signature: (Lorg/gnu/glib/Handle;)J
 */
JNIEXPORT jlong JNICALL Java_org_gnu_gnomevfs_Volume_gnome_1vfs_1volume_1get_1id
  (JNIEnv *env, jclass cls, jobject vol)
{
	GnomeVFSVolume *vol_g = (GnomeVFSVolume*)getPointerFromHandle(env, vol);
	return (jlong)gnome_vfs_volume_get_id(vol_g);
}

/*
 * Class:     org_gnu_gnomevfs_Volume
 * Method:    gnome_vfs_volume_get_volume_type
 * Signature: (Lorg/gnu/glib/Handle;)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_Volume_gnome_1vfs_1volume_1get_1volume_1type
  (JNIEnv *env, jclass cls, jobject vol)
{
	GnomeVFSVolume *vol_g = (GnomeVFSVolume*)getPointerFromHandle(env, vol);
	return (jint)gnome_vfs_volume_get_vol_type(vol_g);
}

/*
 * Class:     org_gnu_gnomevfs_Volume
 * Method:    gnome_vfs_volume_get_device_type
 * Signature: (Lorg/gnu/glib/Handle;)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_Volume_gnome_1vfs_1volume_1get_1device_1type
  (JNIEnv *env, jclass cls, jobject vol)
{
	GnomeVFSVolume *vol_g = (GnomeVFSVolume*)getPointerFromHandle(env, vol);
	return (jint)gnome_vfs_volume_get_device_type(vol_g);
}

/*
 * Class:     org_gnu_gnomevfs_Volume
 * Method:    gnome_vfs_volume_get_drive
 * Signature: (Lorg/gnu/glib/Handle;)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gnomevfs_Volume_gnome_1vfs_1volume_1get_1drive
  (JNIEnv *env, jclass cls, jobject vol)
{
	GnomeVFSVolume *vol_g = (GnomeVFSVolume*)getPointerFromHandle(env, vol);
	return getHandleFromPointer(env, gnome_vfs_volume_get_drive(vol_g));
}

/*
 * Class:     org_gnu_gnomevfs_Volume
 * Method:    gnome_vfs_volume_get_device_path
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Volume_gnome_1vfs_1volume_1get_1device_1path
  (JNIEnv *env, jclass cls, jobject vol)
{
	GnomeVFSVolume *vol_g = (GnomeVFSVolume*)getPointerFromHandle(env, vol);
	return (*env)->NewStringUTF(env, gnome_vfs_volume_get_device_path(vol_g));
}

/*
 * Class:     org_gnu_gnomevfs_Volume
 * Method:    gnome_vfs_volume_get_activation_uri
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Volume_gnome_1vfs_1volume_1get_1activation_1uri
  (JNIEnv *env, jclass cls, jobject vol)
{
	GnomeVFSVolume *vol_g = (GnomeVFSVolume*)getPointerFromHandle(env, vol);
	return (*env)->NewStringUTF(env, gnome_vfs_volume_get_activation_uri(vol_g));
}

/*
 * Class:     org_gnu_gnomevfs_Volume
 * Method:    gnome_vfs_volume_get_filesystem_type
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Volume_gnome_1vfs_1volume_1get_1filesystem_1type
  (JNIEnv *env, jclass cls, jobject vol)
{
	GnomeVFSVolume *vol_g = (GnomeVFSVolume*)getPointerFromHandle(env, vol);
	return (*env)->NewStringUTF(env, gnome_vfs_volume_get_filesystem_type(vol_g));
}

/*
 * Class:     org_gnu_gnomevfs_Volume
 * Method:    gnome_vfs_volume_get_display_name
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Volume_gnome_1vfs_1volume_1get_1display_1name
  (JNIEnv *env, jclass cls, jobject vol)
{
	GnomeVFSVolume *vol_g = (GnomeVFSVolume*)getPointerFromHandle(env, vol);
	return (*env)->NewStringUTF(env, gnome_vfs_volume_get_display_name(vol_g));
}

/*
 * Class:     org_gnu_gnomevfs_Volume
 * Method:    gnome_vfs_volume_get_icon
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Volume_gnome_1vfs_1volume_1get_1icon
  (JNIEnv *env, jclass cls, jobject vol)
{
	GnomeVFSVolume *vol_g = (GnomeVFSVolume*)getPointerFromHandle(env, vol);
	return (*env)->NewStringUTF(env, gnome_vfs_volume_get_icon(vol_g));
}

/*
 * Class:     org_gnu_gnomevfs_Volume
 * Method:    gnome_vfs_volume_get_hal_udi
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gnomevfs_Volume_gnome_1vfs_1volume_1get_1hal_1udi
  (JNIEnv *env, jclass cls, jobject vol)
{
	GnomeVFSVolume *vol_g = (GnomeVFSVolume*)getPointerFromHandle(env, vol);
	return (*env)->NewStringUTF(env, gnome_vfs_volume_get_hal_udi(vol_g));
}

/*
 * Class:     org_gnu_gnomevfs_Volume
 * Method:    gnome_vfs_volume_is_user_visible
 * Signature: (Lorg/gnu/glib/Handle;)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevfs_Volume_gnome_1vfs_1volume_1is_1user_1visible
  (JNIEnv *env, jclass cls, jobject vol)
{
	GnomeVFSVolume *vol_g = (GnomeVFSVolume*)getPointerFromHandle(env, vol);
	return (jboolean)gnome_vfs_volume_is_user_visible(vol_g);
}

/*
 * Class:     org_gnu_gnomevfs_Volume
 * Method:    gnome_vfs_volume_is_read_only
 * Signature: (Lorg/gnu/glib/Handle;)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevfs_Volume_gnome_1vfs_1volume_1is_1read_1only
  (JNIEnv *env, jclass cls, jobject vol)
{
	GnomeVFSVolume *vol_g = (GnomeVFSVolume*)getPointerFromHandle(env, vol);
	return (jboolean)gnome_vfs_volume_is_read_only(vol_g);
}

/*
 * Class:     org_gnu_gnomevfs_Volume
 * Method:    gnome_vfs_volume_is_mounted
 * Signature: (Lorg/gnu/glib/Handle;)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevfs_Volume_gnome_1vfs_1volume_1is_1mounted
  (JNIEnv *env, jclass cls, jobject vol)
{
	GnomeVFSVolume *vol_g = (GnomeVFSVolume*)getPointerFromHandle(env, vol);
	return (jboolean)gnome_vfs_volume_is_mounted(vol_g);
}

/*
 * Class:     org_gnu_gnomevfs_Volume
 * Method:    gnome_vfs_volume_handles_trash
 * Signature: (Lorg/gnu/glib/Handle;)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gnomevfs_Volume_gnome_1vfs_1volume_1handles_1trash
  (JNIEnv *env, jclass cls, jobject vol)
{
	GnomeVFSVolume *vol_g = (GnomeVFSVolume*)getPointerFromHandle(env, vol);
	return (jboolean)gnome_vfs_volume_handles_trash(vol_g);
}

/*
 * Class:     org_gnu_gnomevfs_Volume
 * Method:    gnome_vfs_volume_compare
 * Signature: (Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gnomevfs_Volume_gnome_1vfs_1volume_1compare
  (JNIEnv *env, jclass cls, jobject vola, jobject volb)
{
	GnomeVFSVolume *vola_g = (GnomeVFSVolume*)getPointerFromHandle(env, vola);
	GnomeVFSVolume *volb_g = (GnomeVFSVolume*)getPointerFromHandle(env, volb);
	return (jint)gnome_vfs_volume_compare(vola_g, volb_g);
}

#ifdef __cplusplus
}
#endif
#endif
